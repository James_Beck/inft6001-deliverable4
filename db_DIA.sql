USE master
GO

DROP DATABASE db_DIA
GO

CREATE DATABASE db_DIA
GO

USE db_DIA
GO

CREATE TABLE client (
	cluser VARCHAR(20) PRIMARY KEY,
	clpsswrd VARCHAR(20) NOT NULL,
	clfname VARCHAR(20) NOT NULL,
	cllname VARCHAR(20),
	clemail VARCHAR(20) NOT NULL,
	license VARCHAR(20) NOT NULL,
	clhours INT NOT NULL,
	CONSTRAINT CHK_Client CHECK (clemail LIKE '%@_%.co%' AND clfname NOT LIKE '%[^A-Z]%' AND cllname NOT LIKE '%[^A-Z]%' AND license LIKE 'Full' OR license LIKE 'Learner')
)
GO

CREATE TABLE instructor (
	inuser VARCHAR(20) PRIMARY KEY,
	inpsswrd VARCHAR(20) NOT NULL,
	infname VARCHAR(20) NOT NULL,
	inlname VARCHAR(20),
	inemail VARCHAR(20) NOT NULL,
	inphone VARCHAR(20) NOT NULL,
	workertype VARCHAR(20) NOT NULL,
	deptmng BIT,
	CONSTRAINT CHK_Instructor CHECK (inemail LIKE '%@_%.co%' AND infname NOT LIKE '%[^A-Z]%' AND inlname NOT LIKE '%[^A-Z]%' AND inphone LIKE '%[0-9]%' AND workertype LIKE 'Permanent' OR workertype LIKE 'Casual')
)
GO

CREATE TABLE admin (
	aduser VARCHAR(20) PRIMARY KEY,
	adpsswrd VARCHAR(20) NOT NULL,
	adfname VARCHAR(20) NOT NULL,
	adlname VARCHAR(20),
	ademail VARCHAR(20) NOT NULL,
	adphone VARCHAR(20) NOT NULL,
	CONSTRAINT CHK_Admin CHECK (ademail LIKE '%@_%.co%' AND adfname NOT LIKE '%[^A-Z]%' AND adlname NOT LIKE '%[^A-Z]%' AND adphone LIKE '%[0-9]%')
)
GO

CREATE TABLE bill (
	invoiceID INT IDENTITY(1,1) PRIMARY KEY,
	clfname VARCHAR(20),
	cllname VARCHAR(20),
	amount INT,
	invoicedate VARCHAR(15),
	paid BIT
	)
GO

CREATE TABLE vehicle (
	plate VARCHAR(6) PRIMARY KEY,
	model VARCHAR(200) NOT NULL,
	spare BIT,
	available BIT,
	assigned VARCHAR(20)
)
GO

CREATE TABLE openhours (
	ophours VARCHAR(10) PRIMARY KEY
)
GO

CREATE TABLE maintenance (
	plate VARCHAR(6) PRIMARY KEY,
	FOREIGN KEY (plate) REFERENCES vehicle
)
GO

CREATE TABLE timeoff (
	requesteddatetime VARCHAR(100) PRIMARY KEY,
	infname VARCHAR(20),
	inlname VARCHAR(20),
	confirmed BIT
)
GO

CREATE TABLE appointment (
	apptID INT IDENTITY (1,1) PRIMARY KEY,
	clfname VARCHAR(20),
	cllname VARCHAR(20),
	infname VARCHAR(20),
	inlname VARCHAR(20),
	ophours VARCHAR(10),
	apptdate VARCHAR(15),
	FOREIGN KEY (ophours) REFERENCES openhours
)
GO

INSERT INTO client (cluser, clpsswrd, clfname, cllname, clemail, license, clhours) VALUES
('jd', 'pass1', 'Johnny', 'Depp', 'jd@gmail.com', 'Full', 0),
('bo', 'pass1', 'Barack', 'Obama', 'bo@gmail.com', 'Learner', 0),
('dt', 'pass1', 'Donald', 'Trump', 'dt@gmail.com', 'Learner', 0),
('ep', 'pass1', 'Elvis', 'Presley', 'ep@gmail.com', 'Full', 0),
('al', 'pass1', 'Abraham', 'Lincoln', 'al@gmail.com', 'Full', 0)

SELECT * FROM client

INSERT INTO bill (clfname, cllname, amount, invoicedate, paid) VALUES
('Johnny', 'Depp', 10, '12/06/2017', 1),
('Barack', 'Obama', 25, '12/06/2017', 0),
('Donald', 'Trump', 25, '12/06/2017', 0),
('Elvis', 'Presley', 10, '12/06/2017', 0),
('Abraham', 'Lincoln', 10, '12/06/2017', 0)

SELECT * FROM bill

INSERT INTO instructor (inuser, inpsswrd, infname, inlname, inemail, inphone, workertype, deptmng) VALUES
('ow', 'hell1', 'Oprah', 'Winfrey', 'ow@gmail.com', '111', 'Permanent', 1),
('jk', 'hell2', 'John', 'Kennedy', 'jk@gmail.com', '222', 'Permanent', 0),
('m', 'hell3', 'Madonna', NULL, 'm@gmail.com', '333', 'Permanent', 0),
('db', 'hell4', 'David', 'Beckham', 'db@gmail.com', '444', 'Casual', 0),
('kc', 'hell5', 'Kurt', 'Cobain', 'kc@gmail.com', '555', 'Casual', 0),
('ph', 'hell6', 'Paris', 'Hilton', 'ph@gmail.com', '666', 'Casual', 0),
('bc', 'hell7', 'Bill', 'Clinton', 'bc@gmail.com', '777', 'Casual', 0),
('ts', 'hell8', 'Tupac', 'Shakur', 'ts@gmail.com', '888', 'Casual', 0)

SELECT * FROM instructor

INSERT INTO admin (aduser, adpsswrd, adfname, adlname, ademail, adphone) VALUES
('ss', 'heaven1', 'Steven', 'Smith', 'ss@gmail.com', '1010'),
('lb', 'heaven2', 'Lisa', 'Batey', 'lb@gmail.com', '2121')

SELECT * FROM admin

INSERT INTO timeoff (requesteddatetime, infname, inlname, confirmed) VALUES
('test value 1', 'Bill', 'Clinton', 0),
('test value 2', 'David', 'Beckham', 0)

SELECT * FROM timeoff

INSERT INTO vehicle (plate, model, spare, available, assigned) VALUES
('eqw809', 'Bentley Continental GT V8 S Convertible', 0, 1, 'blank'),
('yrd179', 'Audi RS 7', 0, 1, 'blank'),
('qwm997', 'Aston Martin Vanquish Volante', 0, 1, 'blank'),
('nma796', 'Lamborghini Huracan LP 610-4', 0, 1, 'blank'),
('pua292', 'BMW i8', 1, 1, 'blank')

SELECT * FROM vehicle

DECLARE @hour INT = 7;
WHILE @hour < 10
	BEGIN
		INSERT INTO openhours VALUES ('0' + CONVERT(VARCHAR(2), @hour) + ':00')
		SET @hour = @hour + 1
	END
WHILE @hour < 20
	BEGIN
		INSERT INTO openhours VALUES (CONVERT(VARCHAR(2), @hour) + ':00')
		SET @hour = @hour + 1
	END

SELECT * FROM openhours

INSERT INTO appointment (clfname, cllname, infname, inlname, ophours, apptdate) VALUES
('Johnny', 'Depp', 'John', 'Kennedy', '08:00', '28/07/2017'),
('Johnny', 'Depp', 'John', 'Kennedy', '09:00', '28/07/2017'),
('Johnny', 'Depp', 'Madonna', '', '09:00', '29/07/2017'),
('Barack', 'Obama', 'David', 'Beckham', '10:00', '30/07/2017'),
('Barack', 'Obama', 'David', 'Beckham', '11:00', '30/07/2017'),
('Barack', 'Obama', 'David', 'Beckham', '12:00', '30/07/2017'),
('Barack', 'Obama', 'David', 'Beckham', '13:00', '30/07/2017'),
('Barack', 'Obama', 'David', 'Beckham', '14:00', '30/07/2017'),
('Barack', 'Obama', 'Kurt', 'Cobain', '11:00', '01/08/2017'),
('Donald', 'Trump', 'Paris', 'Hilton', '12:00', '02/08/2017'),
('Donald', 'Trump', 'Bill', 'Clinton', '13:00', '03/08/2017'),
('Elvis', 'Presley', 'Tupac', 'Shakur', '14:00', '04/08/2017'),
('Elvis', 'Presley', 'John', 'Kennedy', '15:00', '05/08/2017'),
('Abraham', 'Lincoln', 'Madonna', '', '16:00', '06/08/2017'),
('Abraham', 'Lincoln', 'David', '	Beckham', '17:00', '07/08/2017')

SELECT * FROM appointment