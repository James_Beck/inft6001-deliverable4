﻿namespace DIA
{
    public class Logout
    {
        public static void loggedout()
        {
            CurrentAdminInfo.Username = null;
            CurrentAdminInfo.Fname = null;
            CurrentAdminInfo.Lname = null;
            CurrentAdminInfo.Email = null;
            CurrentAdminInfo.Phone = null;

            CurrentInstructorInfo.Username = null;
            CurrentInstructorInfo.Fname = null;
            CurrentInstructorInfo.Lname = null;
            CurrentInstructorInfo.Email = null;
            CurrentInstructorInfo.Phone = null;
            CurrentInstructorInfo.Workertype = null;
            CurrentInstructorInfo.deptmng = false;

            CurrentClientInfo.Username = null;
            CurrentClientInfo.Fname = null;
            CurrentClientInfo.Lname = null;
            CurrentClientInfo.Email = null;
            CurrentClientInfo.License = null;
            CurrentClientInfo.Hours = 0;
        }
    }
}
