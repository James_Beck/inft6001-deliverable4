﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DIA
{
    class SQL
    {
        public static SqlConnection con = new SqlConnection(@"Data Source=(LocalDb)\local;Database=db_DIA;Integrated Security=True");
        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataReader see;

        //Method that allows queries to the database like update, delete, and insert
        public static void executeQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
        }

        //Method that allows select queries to the database
        public static void selectQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                see = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
        }
    }
}
