﻿namespace DIA
{
    partial class ScheduleAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Calendar = new System.Windows.Forms.MonthCalendar();
            this.ChooseHour = new System.Windows.Forms.TrackBar();
            this.HourValue = new System.Windows.Forms.Label();
            this.InstructorsList = new System.Windows.Forms.ComboBox();
            this.Schedule = new System.Windows.Forms.Button();
            this.toCliHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseHour)).BeginInit();
            this.SuspendLayout();
            // 
            // Calendar
            // 
            this.Calendar.CalendarDimensions = new System.Drawing.Size(3, 2);
            this.Calendar.Location = new System.Drawing.Point(533, 166);
            this.Calendar.MaxSelectionCount = 1;
            this.Calendar.Name = "Calendar";
            this.Calendar.TabIndex = 0;
            this.Calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.Calendar_DateChanged);
            // 
            // ChooseHour
            // 
            this.ChooseHour.Location = new System.Drawing.Point(533, 552);
            this.ChooseHour.Maximum = 19;
            this.ChooseHour.Minimum = 8;
            this.ChooseHour.Name = "ChooseHour";
            this.ChooseHour.Size = new System.Drawing.Size(689, 45);
            this.ChooseHour.TabIndex = 1;
            this.ChooseHour.Value = 8;
            this.ChooseHour.Scroll += new System.EventHandler(this.ChooseHour_Scroll);
            // 
            // HourValue
            // 
            this.HourValue.AutoSize = true;
            this.HourValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.HourValue.Location = new System.Drawing.Point(822, 505);
            this.HourValue.Name = "HourValue";
            this.HourValue.Size = new System.Drawing.Size(102, 39);
            this.HourValue.TabIndex = 2;
            this.HourValue.Text = "08:00";
            // 
            // InstructorsList
            // 
            this.InstructorsList.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.InstructorsList.FormattingEnabled = true;
            this.InstructorsList.Location = new System.Drawing.Point(533, 620);
            this.InstructorsList.Name = "InstructorsList";
            this.InstructorsList.Size = new System.Drawing.Size(689, 39);
            this.InstructorsList.TabIndex = 3;
            // 
            // Schedule
            // 
            this.Schedule.BackColor = System.Drawing.Color.DarkGreen;
            this.Schedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Schedule.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Schedule.Location = new System.Drawing.Point(533, 693);
            this.Schedule.Name = "Schedule";
            this.Schedule.Size = new System.Drawing.Size(689, 52);
            this.Schedule.TabIndex = 4;
            this.Schedule.Text = "Schedule Appointment";
            this.Schedule.UseVisualStyleBackColor = false;
            this.Schedule.Click += new System.EventHandler(this.Schedule_Click);
            // 
            // toCliHome
            // 
            this.toCliHome.Location = new System.Drawing.Point(0, 0);
            this.toCliHome.Name = "toCliHome";
            this.toCliHome.Size = new System.Drawing.Size(75, 23);
            this.toCliHome.TabIndex = 15;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 14;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // ScheduleAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toCliHome);
            this.Controls.Add(this.Schedule);
            this.Controls.Add(this.InstructorsList);
            this.Controls.Add(this.HourValue);
            this.Controls.Add(this.ChooseHour);
            this.Controls.Add(this.Calendar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScheduleAppointment";
            this.Text = "ScheduleAppointment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ChooseHour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar Calendar;
        private System.Windows.Forms.TrackBar ChooseHour;
        private System.Windows.Forms.Label HourValue;
        private System.Windows.Forms.ComboBox InstructorsList;
        private System.Windows.Forms.Button Schedule;
        private System.Windows.Forms.Button toCliHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}