﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class PayBill : Form
    {
        public static int topay { get; set; }

        public PayBill()
        {
            InitializeComponent();
            owed();
            fillmenu();            
        }

        private void owed()
        {
            SQL.selectQuery($"SELECT * FROM bill WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    LabOwed.Text = $"Amount Owed: ${SQL.see[3]}";
                    topay = (int)SQL.see[3];
                }
            }
        }

        private void Pay_Click(object sender, EventArgs e)
        {
            if (moneyin.Text == "")
            {
                MessageBox.Show("You haven't paid anything");
                return;
            }
            else if (!moneyin.Text.All(Char.IsDigit))
            {
                MessageBox.Show("You cannot input letters");
                return;
            }
            else if (int.Parse(moneyin.Text) < topay)
            {
                MessageBox.Show($"You owe {topay} and are {topay - int.Parse(moneyin.Text)} short. Start again");
                return;
            }
            else if (int.Parse(moneyin.Text) == topay)
            {
                MessageBox.Show("Perfect change, your bill has been paid. Thank you");
                SQL.executeQuery($"UPDATE bill SET paid = 1 WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");
                return;
            }
            else if (int.Parse(moneyin.Text) > topay)
            {
                MessageBox.Show($"You've overpaid. The remaining {int.Parse(moneyin.Text) - topay} has been refunded. Your bill is paid. Thank you");
                SQL.executeQuery($"UPDATE bill SET paid = 1 WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");
                return;
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    ScheduleAppointment schappt = new ScheduleAppointment();
                    schappt.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    Bill tobill = new Bill();
                    tobill.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    PayBill topaybill = new PayBill();
                    topaybill.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Certificate cert = new Certificate();
                    cert.ShowDialog();
                    Close();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Schedule Appointment");
            transmenu.Add("View Bill");
            transmenu.Add("Pay Bill");
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Certificate");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
