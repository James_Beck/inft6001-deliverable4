﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class ScheduleAppointment : Form
    {
        public ScheduleAppointment()
        {
            InitializeComponent();
            loadinfo();
            fillmenu();
        }

        private void ChooseHour_Scroll(object sender, EventArgs e)
        {
            if (ChooseHour.Value < 10)
            {
                HourValue.Text = $"0{ChooseHour.Value}:00";
            }
            else
            {
                HourValue.Text = $"{ChooseHour.Value}:00";
            }
            
            loadinfo();
        }

        private void loadinfo()
        {
            InstructorsList.Items.Clear();
            InstructorsList.Text = null;

            //adds all instructors to the list
            SQL.selectQuery($"SELECT i.infname FROM instructor i WHERE i.infname NOT IN (SELECT a.infname FROM appointment a WHERE a.ophours LIKE '{HourValue.Text}' AND a.apptdate LIKE '{Calendar.SelectionStart.ToString().Substring(0, 10).Trim()}')");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    InstructorsList.Items.Add(SQL.see[0]);
                }
            }   
        }

        private void Schedule_Click(object sender, EventArgs e)
        {
            //prevent same day or in the past appointments scheduled
            if (Calendar.SelectionStart < DateTime.Now)
            {
                MessageBox.Show("You cannot schedule appointments in the past or today");
                return;
            }
            //prevent not selecting instructor
            else if (InstructorsList.SelectedItem == null)
            {
                MessageBox.Show("You must select an instructor");
                return;
            }
            //search for appointments at the same time
            {
                SQL.selectQuery($"SELECT * FROM appointment WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}' AND ophours LIKE '{HourValue.Text}' AND apptdate LIKE '{Calendar.SelectionStart.ToString().Substring(0, 10).Trim()}'");

                if (SQL.see.HasRows)
                {
                    MessageBox.Show($"{CurrentClientInfo.Fname} you have already booked an appointment for this time");
                    return;
                }
            }

            //store the first and last name of the selected instructor
            
            
            //inserts an appointment
            SQL.executeQuery($"INSERT INTO appointment VALUES ('{CurrentClientInfo.Fname}', '{CurrentClientInfo.Lname}', '{InstructorsList.SelectedItem}', 'NULL', '{HourValue.Text}', '{Calendar.SelectionStart.ToString().Substring(0, 10).Trim()}')");

            //check that it has been inserted
            SQL.selectQuery($"SELECT * FROM appointment WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}' AND ophours LIKE '{HourValue.Text}' AND apptdate LIKE '{Calendar.SelectionStart.ToString().Substring(0, 10).Trim()}'");

            if (SQL.see.HasRows)
            {
                MessageBox.Show("Your appointment has been registered");
                StreamWriter email = new StreamWriter(@"C:\users\james\Desktop\inft6001-deliverable4\TestEmailFile.Txt");
                email.Write($"{CurrentClientInfo.Fname} {CurrentClientInfo.Lname} have an appointment at {HourValue.Text} on {Calendar.SelectionStart.ToString().Substring(0, 10).Trim()}, just to let you know.");
                email.Close();
                email.Dispose();
                return;
            }
        }

        private void Calendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            loadinfo();
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    ScheduleAppointment schappt = new ScheduleAppointment();
                    schappt.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    Bill tobill = new Bill();
                    tobill.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    PayBill topaybill = new PayBill();
                    topaybill.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Certificate cert = new Certificate();
                    cert.ShowDialog();
                    Close();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Schedule Appointment");
            transmenu.Add("View Bill");
            transmenu.Add("Pay Bill");
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Certificate");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
