﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class Bill : Form
    {
        public Bill()
        {
            InitializeComponent();
            fillmenu();

            SQL.selectQuery($"SELECT * FROM bill WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    invoicenum.Text = $"Invoice Number: #{SQL.see[0].ToString()}";
                    fullname.Text = $"Amount payable by: {SQL.see[1].ToString()} {SQL.see[2].ToString()}";
                    amount.Text = $"to the value of ${SQL.see[3].ToString()}";
                    dateissue.Text = $"issued on: {SQL.see[4].ToString()}";
                    paid.Text = $"Status of bill payment: Currently Paid - {SQL.see[5].ToString()}";
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    ScheduleAppointment schappt = new ScheduleAppointment();
                    schappt.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    Bill tobill = new Bill();
                    tobill.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    PayBill topaybill = new PayBill();
                    topaybill.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Certificate cert = new Certificate();
                    cert.ShowDialog();
                    Close();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Schedule Appointment");
            transmenu.Add("View Bill");
            transmenu.Add("Pay Bill");
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Certificate");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
