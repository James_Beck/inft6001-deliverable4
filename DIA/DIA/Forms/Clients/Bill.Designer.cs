﻿namespace DIA
{
    partial class Bill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.invoicenum = new System.Windows.Forms.Label();
            this.fullname = new System.Windows.Forms.Label();
            this.amount = new System.Windows.Forms.Label();
            this.paid = new System.Windows.Forms.Label();
            this.dateissue = new System.Windows.Forms.Label();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // invoicenum
            // 
            this.invoicenum.AutoSize = true;
            this.invoicenum.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.invoicenum.Location = new System.Drawing.Point(1459, 48);
            this.invoicenum.Name = "invoicenum";
            this.invoicenum.Size = new System.Drawing.Size(192, 39);
            this.invoicenum.TabIndex = 0;
            this.invoicenum.Text = "Invoicenum";
            // 
            // fullname
            // 
            this.fullname.AutoSize = true;
            this.fullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.fullname.Location = new System.Drawing.Point(564, 235);
            this.fullname.Name = "fullname";
            this.fullname.Size = new System.Drawing.Size(119, 46);
            this.fullname.TabIndex = 1;
            this.fullname.Text = "name";
            // 
            // amount
            // 
            this.amount.AutoSize = true;
            this.amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.amount.Location = new System.Drawing.Point(564, 321);
            this.amount.Name = "amount";
            this.amount.Size = new System.Drawing.Size(153, 46);
            this.amount.TabIndex = 3;
            this.amount.Text = "amount";
            // 
            // paid
            // 
            this.paid.AutoSize = true;
            this.paid.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.paid.Location = new System.Drawing.Point(564, 498);
            this.paid.Name = "paid";
            this.paid.Size = new System.Drawing.Size(95, 46);
            this.paid.TabIndex = 4;
            this.paid.Text = "paid";
            // 
            // dateissue
            // 
            this.dateissue.AutoSize = true;
            this.dateissue.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.dateissue.Location = new System.Drawing.Point(564, 403);
            this.dateissue.Name = "dateissue";
            this.dateissue.Size = new System.Drawing.Size(223, 46);
            this.dateissue.TabIndex = 5;
            this.dateissue.Text = "date issued";
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 12;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // Bill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.dateissue);
            this.Controls.Add(this.paid);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.fullname);
            this.Controls.Add(this.invoicenum);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Bill";
            this.Text = "Bill";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label invoicenum;
        private System.Windows.Forms.Label fullname;
        private System.Windows.Forms.Label amount;
        private System.Windows.Forms.Label paid;
        private System.Windows.Forms.Label dateissue;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}