﻿namespace DIA
{
    partial class Certificate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabAward = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.voidaward = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 120F);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(637, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(799, 181);
            this.label1.TabIndex = 0;
            this.label1.Text = "Certificate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 70F);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(515, 330);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1012, 107);
            this.label2.TabIndex = 1;
            this.label2.Text = "Of Driving Competency";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(520, 643);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(411, 76);
            this.label3.TabIndex = 2;
            this.label3.Text = "Awarded To:";
            // 
            // LabAward
            // 
            this.LabAward.AutoSize = true;
            this.LabAward.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F);
            this.LabAward.Location = new System.Drawing.Point(937, 631);
            this.LabAward.Name = "LabAward";
            this.LabAward.Size = new System.Drawing.Size(0, 91);
            this.LabAward.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(370, 888);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1229, 76);
            this.label4.TabIndex = 4;
            this.label4.Text = "For Their Showing Of Driving Excellence";
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 5;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // voidaward
            // 
            this.voidaward.AutoSize = true;
            this.voidaward.Font = new System.Drawing.Font("Microsoft Sans Serif", 140F);
            this.voidaward.ForeColor = System.Drawing.Color.Tomato;
            this.voidaward.Location = new System.Drawing.Point(24, 437);
            this.voidaward.Name = "voidaward";
            this.voidaward.Size = new System.Drawing.Size(1855, 211);
            this.voidaward.TabIndex = 6;
            this.voidaward.Text = "Currently Unawarded";
            // 
            // Certificate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Goldenrod;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.voidaward);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LabAward);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Certificate";
            this.Text = "Certificate";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabAward;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox TransitionalMenu;
        private System.Windows.Forms.Label voidaward;
    }
}