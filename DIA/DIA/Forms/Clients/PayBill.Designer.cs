﻿namespace DIA
{
    partial class PayBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.moneyin = new System.Windows.Forms.TextBox();
            this.LabOwed = new System.Windows.Forms.Label();
            this.Pay = new System.Windows.Forms.Button();
            this.toCliHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // moneyin
            // 
            this.moneyin.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.moneyin.Location = new System.Drawing.Point(655, 396);
            this.moneyin.Name = "moneyin";
            this.moneyin.Size = new System.Drawing.Size(420, 45);
            this.moneyin.TabIndex = 0;
            // 
            // LabOwed
            // 
            this.LabOwed.AutoSize = true;
            this.LabOwed.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabOwed.Location = new System.Drawing.Point(648, 328);
            this.LabOwed.Name = "LabOwed";
            this.LabOwed.Size = new System.Drawing.Size(258, 39);
            this.LabOwed.TabIndex = 1;
            this.LabOwed.Text = "Amount Owing: ";
            // 
            // Pay
            // 
            this.Pay.BackColor = System.Drawing.Color.DarkGreen;
            this.Pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Pay.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Pay.Location = new System.Drawing.Point(655, 479);
            this.Pay.Name = "Pay";
            this.Pay.Size = new System.Drawing.Size(420, 63);
            this.Pay.TabIndex = 2;
            this.Pay.Text = "Make Payment";
            this.Pay.UseVisualStyleBackColor = false;
            this.Pay.Click += new System.EventHandler(this.Pay_Click);
            // 
            // toCliHome
            // 
            this.toCliHome.Location = new System.Drawing.Point(0, 0);
            this.toCliHome.Name = "toCliHome";
            this.toCliHome.Size = new System.Drawing.Size(75, 23);
            this.toCliHome.TabIndex = 14;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 13;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // PayBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toCliHome);
            this.Controls.Add(this.Pay);
            this.Controls.Add(this.LabOwed);
            this.Controls.Add(this.moneyin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PayBill";
            this.Text = "PayBill";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox moneyin;
        private System.Windows.Forms.Label LabOwed;
        private System.Windows.Forms.Button Pay;
        private System.Windows.Forms.Button toCliHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}