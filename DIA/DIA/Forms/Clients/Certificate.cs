﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class Certificate : Form
    {
        public static bool paid { get; set; }

        public Certificate()
        {
            InitializeComponent();
            fillmenu();
            LabAward.Text = $"{CurrentClientInfo.Fname} {CurrentClientInfo.Lname}";
        }

        private void certearned()
        {
            //get bill info
            SQL.selectQuery($"SELECT * FROM bill WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    paid = (bool)SQL.see[5];
                }
            }

            //check all relevant certificate criteria
            if (CurrentClientInfo.License == "Full" && CurrentClientInfo.Hours == 2 && paid || CurrentClientInfo.License == "Learner" && CurrentClientInfo.Hours == 5 && paid)
            {
                voidaward.Hide();
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    ScheduleAppointment schappt = new ScheduleAppointment();
                    schappt.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    Bill tobill = new Bill();
                    tobill.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    PayBill topaybill = new PayBill();
                    topaybill.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Certificate cert = new Certificate();
                    cert.ShowDialog();
                    Close();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Schedule Appointment");
            transmenu.Add("View Bill");
            transmenu.Add("Pay Bill");
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Certificate");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
