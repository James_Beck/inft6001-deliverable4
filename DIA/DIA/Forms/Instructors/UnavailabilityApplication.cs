﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class UnavailabilityApplication : Form
    {
        public int counter = 0;
        public long daysnow, firstdays, lastdays;
        public int firstchoice, secondchoice;
        public string timeslot1, timeslot2;

        public UnavailabilityApplication()
        {
            InitializeComponent();
            fillmenu();

            //measures the number of days from current day
            DateTime now = DateTime.Now;
            int thisyear = int.Parse(now.ToString().Substring(6, 4));
            int thismonth = int.Parse(now.ToString().Substring(3, 2));
            int thisday = int.Parse(now.ToString().Substring(0, 2));
            daysnow = thisyear * 365 + thismonth * 30 + thisday;
        }      

        private void HourSelect_Scroll(object sender, EventArgs e)
        {
            //communicated time selected
            TestChange.Text = HourSelect.Value.ToString() + ":00";
        }

        private void Confirm_Click(object sender, EventArgs e)
        {
            counter++;
            if (counter == 1)
            {
                //set value firstchoice
                firstchoice = HourSelect.Value;

                //measures the number of days from selected day
                DateTime first = Calendar.SelectionRange.Start;
                int selectyear = first.Year;
                int selectmonth = first.Month;
                int selectday = first.Day;
                firstdays = selectyear * 365 + selectmonth * 30 + selectday;
                
                //check for emergency timeoff request
                if (Check.Checked)
                {
                    //check the dates, if less than 0 it's wrong
                    if (firstdays < daysnow)
                    {
                        MessageBox.Show("Why are you trying to select time off for days past?");
                        counter--;
                        return;
                    }
                }
                else
                {
                    if (firstdays - daysnow < 45)
                    {
                        MessageBox.Show("You can only request time off 45 days or more in advance");
                        counter--;
                        return;
                    }
                }
                
                LabStart.Text = $"Unavailable: {Calendar.SelectionStart.ToString().Substring(0, 10)}  from  {TestChange.Text}";
                timeslot1 = $"({Calendar.SelectionStart.ToString().Substring(0, 10)}: {TestChange.Text}) - ";
            }
            else if (counter == 2)
            {
                //set value secondchoice
                secondchoice = HourSelect.Value;

                //measures the number of days from selected day
                DateTime end = Calendar.SelectionRange.End;
                int selectyear = end.Year;
                int selectmonth = end.Month;
                int selectday = end.Day;
                lastdays = selectyear * 365 + selectmonth * 30 + selectday;

                //check for that lastdays is after firstdays
                if (lastdays == firstdays)
                {
                    //checks for an incorrect difference in time
                    if (firstchoice >= secondchoice)
                    {
                        MessageBox.Show("This isn't any time off");
                        counter--;
                        return;
                    }
                }
                //check for a negative difference in days
                else if (lastdays < firstdays)
                {
                    MessageBox.Show("You can't take off negative time");
                    counter--;
                    return;
                }

                Confirm.Text = "Confirm?";
                LabEnd.Text = $"Available:    {Calendar.SelectionStart.ToString().Substring(0, 10)}  from  {TestChange.Text}";
                timeslot2 = $"({Calendar.SelectionStart.ToString().Substring(0, 10)}: {TestChange.Text})";
            }
            //inserts the time off request and checks that it's there
            else if (counter == 3)
            {
                SQL.selectQuery($"SELECT requesteddatetime FROM timeoff WHERE requesteddatetime LIKE '{timeslot1}{timeslot2}' AND infname LIKE '{CurrentInstructorInfo.Fname}' AND inlname LIKE {CurrentInstructorInfo.Lname}''");

                //check if request has already been made
                if (SQL.see.HasRows)
                {
                    MessageBox.Show("This request has already been made by your account. You cannot make a second one");
                    Startover();
                    return;
                }

                //insert request
                SQL.executeQuery($"INSERT INTO timeoff VALUES('{timeslot1}{timeslot2}', '{CurrentInstructorInfo.Fname}', '{CurrentInstructorInfo.Lname}', '{0}')");

                //check that table was insert
                SQL.selectQuery($"SELECT * FROM timeoff WHERE infname LIKE '{CurrentInstructorInfo.Fname}' AND inlname LIKE '{CurrentInstructorInfo.Lname}'");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {
                        MessageBox.Show($"{SQL.see[0]}, {SQL.see[1]}, {SQL.see[2]}. Requested: Currently Unconfirmed");
                    }
                }
            }
        }

        private void Restart_Click(object sender, EventArgs e)
        {
            Startover();
        }

        //reset information
        private void Startover()
        {
            counter = 0;
            LabStart.Text = "Unavailable:";
            LabEnd.Text = "Available:";
            Confirm.Text = "Choose Date/Time";
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    UnavailabilityApplication timeoff = new UnavailabilityApplication();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    InstructorsAppts grade = new InstructorsAppts();
                    grade.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Apply Time Off");
            transmenu.Add("Grade Appointments");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
