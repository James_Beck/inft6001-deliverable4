﻿namespace DIA
{
    partial class InstructorsAppts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.apptscombobox = new System.Windows.Forms.ComboBox();
            this.Fail = new System.Windows.Forms.Button();
            this.Pass = new System.Windows.Forms.Button();
            this.appIDCombobox = new System.Windows.Forms.ComboBox();
            this.LabAward = new System.Windows.Forms.Label();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // apptscombobox
            // 
            this.apptscombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.apptscombobox.FormattingEnabled = true;
            this.apptscombobox.Location = new System.Drawing.Point(669, 120);
            this.apptscombobox.Name = "apptscombobox";
            this.apptscombobox.Size = new System.Drawing.Size(788, 39);
            this.apptscombobox.TabIndex = 0;
            // 
            // Fail
            // 
            this.Fail.BackColor = System.Drawing.Color.DarkRed;
            this.Fail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Fail.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Fail.Location = new System.Drawing.Point(525, 199);
            this.Fail.Name = "Fail";
            this.Fail.Size = new System.Drawing.Size(376, 62);
            this.Fail.TabIndex = 1;
            this.Fail.Text = "Done and Fail";
            this.Fail.UseVisualStyleBackColor = false;
            this.Fail.Click += new System.EventHandler(this.Fail_Click);
            // 
            // Pass
            // 
            this.Pass.BackColor = System.Drawing.Color.DarkGreen;
            this.Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Pass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Pass.Location = new System.Drawing.Point(1068, 199);
            this.Pass.Name = "Pass";
            this.Pass.Size = new System.Drawing.Size(389, 62);
            this.Pass.TabIndex = 2;
            this.Pass.Text = "Done and Pass";
            this.Pass.UseVisualStyleBackColor = false;
            this.Pass.Click += new System.EventHandler(this.Pass_Click);
            // 
            // appIDCombobox
            // 
            this.appIDCombobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.appIDCombobox.FormattingEnabled = true;
            this.appIDCombobox.Location = new System.Drawing.Point(525, 120);
            this.appIDCombobox.Name = "appIDCombobox";
            this.appIDCombobox.Size = new System.Drawing.Size(118, 39);
            this.appIDCombobox.TabIndex = 10;
            // 
            // LabAward
            // 
            this.LabAward.AutoSize = true;
            this.LabAward.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.LabAward.Location = new System.Drawing.Point(519, 354);
            this.LabAward.Name = "LabAward";
            this.LabAward.Size = new System.Drawing.Size(302, 31);
            this.LabAward.TabIndex = 11;
            this.LabAward.Text = "Certificate Awarded: No";
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 14;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // InstructorsAppts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.LabAward);
            this.Controls.Add(this.appIDCombobox);
            this.Controls.Add(this.Pass);
            this.Controls.Add(this.Fail);
            this.Controls.Add(this.apptscombobox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InstructorsAppts";
            this.Text = "InstructorsAppts";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox apptscombobox;
        private System.Windows.Forms.Button Fail;
        private System.Windows.Forms.Button Pass;
        private System.Windows.Forms.ComboBox appIDCombobox;
        private System.Windows.Forms.Label LabAward;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}