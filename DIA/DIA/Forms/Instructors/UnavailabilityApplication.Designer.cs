﻿namespace DIA
{
    partial class UnavailabilityApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Calendar = new System.Windows.Forms.MonthCalendar();
            this.TestChange = new System.Windows.Forms.Label();
            this.HourSelect = new System.Windows.Forms.TrackBar();
            this.LabStart = new System.Windows.Forms.Label();
            this.LabEnd = new System.Windows.Forms.Label();
            this.Confirm = new System.Windows.Forms.Button();
            this.Restart = new System.Windows.Forms.Button();
            this.Check = new System.Windows.Forms.CheckBox();
            this.toInHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.HourSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // Calendar
            // 
            this.Calendar.CalendarDimensions = new System.Drawing.Size(3, 2);
            this.Calendar.Location = new System.Drawing.Point(261, 150);
            this.Calendar.MaxSelectionCount = 1;
            this.Calendar.Name = "Calendar";
            this.Calendar.TabIndex = 0;
            // 
            // TestChange
            // 
            this.TestChange.AutoSize = true;
            this.TestChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.TestChange.Location = new System.Drawing.Point(559, 526);
            this.TestChange.Name = "TestChange";
            this.TestChange.Size = new System.Drawing.Size(83, 39);
            this.TestChange.TabIndex = 1;
            this.TestChange.Text = "8:00";
            // 
            // HourSelect
            // 
            this.HourSelect.Location = new System.Drawing.Point(261, 596);
            this.HourSelect.Maximum = 19;
            this.HourSelect.Minimum = 8;
            this.HourSelect.Name = "HourSelect";
            this.HourSelect.Size = new System.Drawing.Size(689, 45);
            this.HourSelect.TabIndex = 2;
            this.HourSelect.Value = 8;
            this.HourSelect.Scroll += new System.EventHandler(this.HourSelect_Scroll);
            // 
            // LabStart
            // 
            this.LabStart.AutoSize = true;
            this.LabStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabStart.Location = new System.Drawing.Point(1045, 150);
            this.LabStart.Name = "LabStart";
            this.LabStart.Size = new System.Drawing.Size(206, 39);
            this.LabStart.TabIndex = 3;
            this.LabStart.Text = "Unavailable:";
            // 
            // LabEnd
            // 
            this.LabEnd.AutoSize = true;
            this.LabEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabEnd.Location = new System.Drawing.Point(1045, 260);
            this.LabEnd.Name = "LabEnd";
            this.LabEnd.Size = new System.Drawing.Size(166, 39);
            this.LabEnd.TabIndex = 4;
            this.LabEnd.Text = "Available:";
            // 
            // Confirm
            // 
            this.Confirm.BackColor = System.Drawing.Color.DarkGreen;
            this.Confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Confirm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Confirm.Location = new System.Drawing.Point(1052, 398);
            this.Confirm.Name = "Confirm";
            this.Confirm.Size = new System.Drawing.Size(398, 76);
            this.Confirm.TabIndex = 5;
            this.Confirm.Text = "Choose Date/Time";
            this.Confirm.UseVisualStyleBackColor = false;
            this.Confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // Restart
            // 
            this.Restart.BackColor = System.Drawing.Color.DarkRed;
            this.Restart.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Restart.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Restart.Location = new System.Drawing.Point(1052, 547);
            this.Restart.Name = "Restart";
            this.Restart.Size = new System.Drawing.Size(398, 76);
            this.Restart.TabIndex = 6;
            this.Restart.Text = "Start Again";
            this.Restart.UseVisualStyleBackColor = false;
            this.Restart.Click += new System.EventHandler(this.Restart_Click);
            // 
            // Check
            // 
            this.Check.AutoSize = true;
            this.Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Check.Location = new System.Drawing.Point(932, 743);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(208, 43);
            this.Check.TabIndex = 7;
            this.Check.Text = "Emergency";
            this.Check.UseVisualStyleBackColor = true;
            // 
            // toInHome
            // 
            this.toInHome.Location = new System.Drawing.Point(0, 0);
            this.toInHome.Name = "toInHome";
            this.toInHome.Size = new System.Drawing.Size(75, 23);
            this.toInHome.TabIndex = 16;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 15;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // UnavailabilityApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toInHome);
            this.Controls.Add(this.Check);
            this.Controls.Add(this.Restart);
            this.Controls.Add(this.Confirm);
            this.Controls.Add(this.LabEnd);
            this.Controls.Add(this.LabStart);
            this.Controls.Add(this.HourSelect);
            this.Controls.Add(this.TestChange);
            this.Controls.Add(this.Calendar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UnavailabilityApplication";
            this.Text = "UnavailabilityApplication";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.HourSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar Calendar;
        private System.Windows.Forms.Label TestChange;
        private System.Windows.Forms.TrackBar HourSelect;
        private System.Windows.Forms.Label LabStart;
        private System.Windows.Forms.Label LabEnd;
        private System.Windows.Forms.Button Confirm;
        private System.Windows.Forms.Button Restart;
        private System.Windows.Forms.CheckBox Check;
        private System.Windows.Forms.Button toInHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}