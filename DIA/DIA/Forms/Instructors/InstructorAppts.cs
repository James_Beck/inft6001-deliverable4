﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class InstructorsAppts : Form
    {
        public string changeclifname { get; set; }
        public string changeclilname { get; set; }
        public bool clipaid { get; set; }

        public InstructorsAppts()
        {
            InitializeComponent();
            fillmenu();

            SQL.executeQuery($"UPDATE appointment SET inlname = (SELECT inlname FROM instructor WHERE infname LIKE '{CurrentInstructorInfo.Fname}') WHERE inlname IS NULL");
            appointments();
        }

        private void Fail_Click(object sender, EventArgs e)
        {
            //deletes appointment
            SQL.executeQuery($"DELETE FROM appointment WHERE apptID LIKE {appIDCombobox.Text}");

            //checks that appointment was deleted
            SQL.selectQuery($"SELECT * FROM appointment WHERE apptID LIKE {appIDCombobox.Text}");

            if (!SQL.see.HasRows)
            {
                MessageBox.Show("The appointment has been deleted, customer hours not increased");
            }

            appointments();
        }

        private void Pass_Click(object sender, EventArgs e)
        {
            //updates to show a passed result
            SQL.selectQuery($"SELECT clfname, cllname FROM appointment WHERE apptID LIKE {appIDCombobox.Text}");
            
            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    changeclifname = SQL.see[0].ToString();
                    changeclilname = SQL.see[1].ToString();
                }
            }

            SQL.executeQuery($"UPDATE client SET clhours = clhours + 1 WHERE clfname LIKE '{changeclifname}' and cllname LIKE '{changeclilname}'");

            //deletes appointment
            SQL.executeQuery($"DELETE FROM appointment WHERE apptID LIKE {appIDCombobox.Text}");

            //checks that appointment was deleted
            SQL.selectQuery($"SELECT * FROM appointment WHERE apptID LIKE {appIDCombobox.Text}");

            if (!SQL.see.HasRows)
            {
                MessageBox.Show("The appointment has been deleted, customer hours increased");
            }

            appointments();

            //check to see if a certificate should be awarded
            SQL.selectQuery($"SELECT * FROM bill WHERE clfname LIKE '{changeclifname}' AND cllname LIKE '{changeclilname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    clipaid = (bool)SQL.see[5];
                }
            }

            SQL.selectQuery($"SELECT * FROM client WHERE clfname LIKE '{changeclifname}' AND cllname LIKE '{changeclilname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    if (SQL.see[5].ToString() == "Full" && (int)SQL.see[6] == 2 && clipaid || SQL.see[5].ToString() == "Learner" && (int)SQL.see[6] == 5 && clipaid)
                    {
                        LabAward.Text = "Certificate Awarded: Yes";
                    }
                }
            }
        }

        private void appointments()
        {
            appIDCombobox.Text = null;
            apptscombobox.Items.Clear();
            apptscombobox.Text = null;

            //selects appointments relevant to instructor
            SQL.selectQuery($"SELECT * FROM appointment WHERE infname LIKE '{CurrentInstructorInfo.Fname}' AND inlname LIKE '{CurrentInstructorInfo.Lname}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    appIDCombobox.Text = SQL.see[0].ToString();
                    apptscombobox.Items.Add($"{SQL.see[1]} {SQL.see[2]}: {SQL.see[5]} - {SQL.see[6]} ");
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    UnavailabilityApplication timeoff = new UnavailabilityApplication();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    InstructorsAppts grade = new InstructorsAppts();
                    grade.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("Upcoming Appointments");
            transmenu.Add("Apply Time Off");
            transmenu.Add("Grade Appointments");
            transmenu.Add("---------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}