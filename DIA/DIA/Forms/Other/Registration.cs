﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class Registration : Form
    {
        //determines which button was pressed
        public bool client = false;
        public bool instructor = false;
        public bool admin = false;
        public bool superpass = false;

        public Registration()
        {
            InitializeComponent();
            
            //hides a lot of boxes until they're called on
            LabUsername.Hide();
            UsernameField.Hide();
            LabPassword.Hide();
            PasswordField.Hide();
            LabFName.Hide();
            FNameField.Hide();
            LabLName.Hide();
            LNameField.Hide();
            LabEmail.Hide();
            EmailField.Hide();
            LabLicense.Hide();
            DropLicense.Hide();
            LabPhone.Hide();
            PhoneField.Hide();
            LabWorkType.Hide();
            DropWorkType.Hide();
            LabReqPass.Hide();
            ReqPassField.Hide();

            DropLicense.Items.Add("Full");
            DropLicense.Items.Add("Learner");
            DropWorkType.Items.Add("Casual");
            DropWorkType.Items.Add("Permanent");
        }

        private void Register_Click(object sender, EventArgs e)
        {
            string username, password, fname, lname, email, license, workertype, reqpass, phone;

            username = UsernameField.Text.Trim();
            password = PasswordField.Text.Trim();
            fname = FNameField.Text.Trim();
            lname = LNameField.Text.Trim();
            email = EmailField.Text.Trim();
            license = DropLicense.Text.Trim();
            workertype = DropWorkType.Text.Trim();
            reqpass = ReqPassField.Text.Trim();
            phone = PhoneField.Text.Trim();

            //checks if fields are blank
            if (username == "" || password == "" || fname == "" || email == "")
            {
                MessageBox.Show("Username, Password, Fname, and Email fields cannot be blank");
                return;
            }
            //checks if names contain any digits
            else if (fname.Any(char.IsDigit) || lname.Any(char.IsDigit))
            {
                MessageBox.Show("You can't have numbers in your name");
                return;
            }
            //checks that the email was approximately entered correctly
            else if (!email.Contains("@") && !email.Contains(".co"))
            {
                MessageBox.Show("We don't recognise that to be a proper email");
                return;
            }

            //search database to do some more detailed checks
            string[] arr = { "client", "instructor", "admin" };
            for (int i = 0; i < arr.Count(); i++)
            {
                SQL.selectQuery($"SELECT * FROM {arr[i]}");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {                        
                        //checks all tables for username
                        if (SQL.see[0].ToString() == username)
                        {
                            MessageBox.Show("That username already exists, please try another");
                            return;
                        }
                        //checks for non-digit characters in the phone number
                        else if (!phone.All(char.IsDigit) && arr[i] == "instructor" || !phone.All(char.IsDigit) && arr[i] == "admin")
                        {
                            MessageBox.Show("Phone numbers don't contain letters");
                            return;
                        }
                        //set variable to communicate the correct password is set
                        else if (reqpass == SQL.see[1].ToString() && arr[i] == "admin")
                        {
                            superpass = true;
                        }
                    }
                }
            }

            //check require password is correct
            if (!superpass && !client)
            {
                MessageBox.Show("The required password is incorrect");
                return;
            }

            //which table are we looking at?
            //insert values into the database
            if (client)
            {
                SQL.executeQuery($"INSERT INTO client VALUES ('{username}', '{password}', '{fname}', '{lname}', '{email}', '{license}', 0)");

                int amount = 0;

                if (license == "Full")
                {
                    amount = 10;
                }
                else if (license == "Leaner")
                {
                    amount = 25;
                }

                SQL.executeQuery($"INSERT INTO bill VALUES ('{fname}', '{lname}', '{amount}', '{DateTime.Now.ToString().Substring(0, 10).Trim()}', '0')");
            }
            else if (instructor)
            {
                SQL.executeQuery($"INSERT INTO instructor VALUES ('{username}', '{password}', '{fname}', '{lname}', '{email}', '{phone}', '{workertype}', 0)");
            }
            else if (admin)
            {
                SQL.executeQuery($"INSERT INTO admin VALUES ('{username}', '{password}', '{fname}', '{lname}', '{email}', '{phone}')");
            }

            //check that new account has been registered
            if (client)
            {
                SQL.selectQuery($"SELECT cluser FROM client WHERE cluser LIKE '{username}'");

                if (SQL.see.HasRows)
                {
                    MessageBox.Show($"You've successfully registered an account as {username}");
                    return;
                }
                else
                {
                    MessageBox.Show("Something unexpected has gone wrong. Please try again");
                    return;
                }
            }
            else if (instructor)
            {
                SQL.selectQuery($"SELECT inuser FROM instructor WHERE inuser LIKE '{username}'");

                if (SQL.see.HasRows)
                {
                    MessageBox.Show($"You've successfully registered an account as {username}");
                    return;
                }
                else
                {
                    MessageBox.Show("Something unexpected has gone wrong. Please try again");
                    return;
                }
            }
            else if (admin)
            {
                SQL.selectQuery($"SELECT aduser FROM admin WHERE aduser LIKE '{username}'");

                if (SQL.see.HasRows)
                {
                    MessageBox.Show($"You've successfully registered an account as {username}");
                    return;
                }
                else
                {
                    MessageBox.Show("Something unexpected has gone wrong. Please try again");
                    return;
                }
            }
        }

        private void RegClient_Click(object sender, EventArgs e)
        {
            client = true;
            instructor = false;
            admin = false;
            LabUsername.Show();
            UsernameField.Show();
            LabPassword.Show();
            PasswordField.Show();
            LabFName.Show();
            FNameField.Show();
            LabLName.Show();
            LNameField.Show();
            LabEmail.Show();
            EmailField.Show();
            LabLicense.Show();
            DropLicense.Show();
            LabPhone.Hide();
            PhoneField.Hide();
            LabWorkType.Hide();
            DropWorkType.Hide();
            LabReqPass.Hide();
            ReqPassField.Hide();
        }

        private void RegInstructor_Click(object sender, EventArgs e)
        {
            client = false;
            instructor = true;
            admin = false;
            LabUsername.Show();
            UsernameField.Show();
            LabPassword.Show();
            PasswordField.Show();
            LabFName.Show();
            FNameField.Show();
            LabLName.Show();
            LNameField.Show();
            LabEmail.Show();
            EmailField.Show();
            LabLicense.Hide();
            DropLicense.Hide();
            LabPhone.Show();
            PhoneField.Show();
            LabWorkType.Show();
            DropWorkType.Show();
            LabReqPass.Show();
            ReqPassField.Show();
        }

        private void RegAdmin_Click(object sender, EventArgs e)
        {
            client = false;
            instructor = false;
            admin = true;
            LabUsername.Show();
            UsernameField.Show();
            LabPassword.Show();
            PasswordField.Show();
            LabFName.Show();
            FNameField.Show();
            LabLName.Show();
            LNameField.Show();
            LabEmail.Show();
            EmailField.Show();
            LabLicense.Hide();
            DropLicense.Hide();
            LabPhone.Show();
            PhoneField.Show();
            LabWorkType.Hide();
            DropWorkType.Hide();
            LabReqPass.Show();
            ReqPassField.Show();
        }

        private void toLogin_Click(object sender, EventArgs e)
        {
            Hide();
            Home tologin = new Home();
            tologin.ShowDialog();
            Close();
        }
    }
}
