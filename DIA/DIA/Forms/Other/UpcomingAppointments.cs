﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class UpcomingAppointments : Form
    {
        public UpcomingAppointments()
        {
            InitializeComponent();
            appointmentinfo();
            fillmenu();
        }

        private void appointmentinfo()
        {
            //check for log in
            if (CurrentInstructorInfo.Username != null)
            {
                //get relevant appointments
                SQL.selectQuery($"SELECT * FROM appointment WHERE infname LIKE '{CurrentInstructorInfo.Fname}' AND inlname LIKE '{CurrentInstructorInfo.Lname}'");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {
                        Appointments.Items.Add($"{SQL.see[1]} {SQL.see[2]} - {SQL.see[3]} {SQL.see[4]} - {SQL.see[5]}: {SQL.see[6]}");
                    }
                }                
            }
            else if (CurrentClientInfo.Username != null)
            {
                //get relevant appointments
                SQL.selectQuery($"SELECT * FROM appointment WHERE clfname LIKE '{CurrentClientInfo.Fname}' AND cllname LIKE '{CurrentClientInfo.Lname}'");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {
                        Appointments.Items.Add($"{SQL.see[1]} {SQL.see[2]} - {SQL.see[3]} {SQL.see[4]} - {SQL.see[5]}: {SQL.see[6]}");
                    }
                }
            }
            else
            {
                //get all appointments
                SQL.selectQuery("SELECT * FROM appointment");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {
                        Appointments.Items.Add($"{SQL.see[1]} {SQL.see[2]} - {SQL.see[3]} {SQL.see[4]} - {SQL.see[5]}: {SQL.see[6]}");
                    }
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentClientInfo.Username != null)
            {
                switch (TransitionalMenu.SelectedIndex)
                {
                    case 0:
                        Hide();
                        ScheduleAppointment schappt = new ScheduleAppointment();
                        schappt.ShowDialog();
                        Close();
                        break;
                    case 1:
                        Hide();
                        Bill tobill = new Bill();
                        tobill.ShowDialog();
                        Close();
                        break;
                    case 2:
                        Hide();
                        PayBill topaybill = new PayBill();
                        topaybill.ShowDialog();
                        Close();
                        break;
                    case 3:
                        Hide();
                        UpcomingAppointments toappts = new UpcomingAppointments();
                        toappts.ShowDialog();
                        Close();
                        break;
                    case 4:
                        Hide();
                        Certificate cert = new Certificate();
                        cert.ShowDialog();
                        Close();
                        break;
                    case 6:
                        Hide();
                        Home home = new Home();
                        home.ShowDialog();
                        Close();
                        break;
                }
            }
            else if (CurrentInstructorInfo.Username != null)
            {
                switch (TransitionalMenu.SelectedIndex)
                {
                    case 0:
                        Hide();
                        UpcomingAppointments toappts = new UpcomingAppointments();
                        toappts.ShowDialog();
                        Close();
                        break;
                    case 1:
                        Hide();
                        UnavailabilityApplication timeoff = new UnavailabilityApplication();
                        timeoff.ShowDialog();
                        Close();
                        break;
                    case 2:
                        Hide();
                        InstructorsAppts grade = new InstructorsAppts();
                        grade.ShowDialog();
                        Close();
                        break;
                    case 4:
                        Hide();
                        Home home = new Home();
                        home.ShowDialog();
                        Close();
                        break;
                }
            }
            else
            {
                switch (TransitionalMenu.SelectedIndex)
                {
                    case 0:
                        Hide();
                        UpcomingAppointments toappts = new UpcomingAppointments();
                        toappts.ShowDialog();
                        Close();
                        break;
                    case 1:
                        Hide();
                        AssignCar cars = new AssignCar();
                        cars.ShowDialog();
                        Close();
                        break;
                    case 2:
                        Hide();
                        ConfirmUnavailability timeoff = new ConfirmUnavailability();
                        timeoff.ShowDialog();
                        Close();
                        break;
                    case 3:
                        Hide();
                        DeleteStaff delete = new DeleteStaff();
                        delete.ShowDialog();
                        Close();
                        break;
                    case 4:
                        Hide();
                        Statistics stats = new Statistics();
                        stats.ShowDialog();
                        break;
                    case 6:
                        Hide();
                        Home home = new Home();
                        home.ShowDialog();
                        Close();
                        break;
                }
            }
        }

        private void fillmenu()
        {
            if (CurrentClientInfo.Username != null)
            {
                List<string> transmenu = new List<string>();
                transmenu.Add("Schedule Appointment");
                transmenu.Add("View Bill");
                transmenu.Add("Pay Bill");
                transmenu.Add("Upcoming Appointments");
                transmenu.Add("Certificate");
                transmenu.Add("---------------------");
                transmenu.Add("Logout");

                for (int i = 0; i < transmenu.Count(); i++)
                {
                    TransitionalMenu.Items.Add(transmenu[i]);
                }
            }
            else if (CurrentInstructorInfo.Username != null)
            {
                List<string> transmenu = new List<string>();
                transmenu.Add("Upcoming Appointments");
                transmenu.Add("Apply Time Off");
                transmenu.Add("Grade Appointments");
                transmenu.Add("---------------------");
                transmenu.Add("Logout");

                for (int i = 0; i < transmenu.Count(); i++)
                {
                    TransitionalMenu.Items.Add(transmenu[i]);
                }
            }
            else
            {
                List<string> transmenu = new List<string>();
                transmenu.Add("All Appointments");
                transmenu.Add("Assign Cars");
                transmenu.Add("Time Off Requests");
                transmenu.Add("Delete Staff Account");
                transmenu.Add("Company Statistics");
                transmenu.Add("--------------------");
                transmenu.Add("Logout");

                for (int i = 0; i < transmenu.Count(); i++)
                {
                    TransitionalMenu.Items.Add(transmenu[i]);
                }
            }
        }
    }
}
