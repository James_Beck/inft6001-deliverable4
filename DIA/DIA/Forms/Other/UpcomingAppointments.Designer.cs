﻿namespace DIA
{
    partial class UpcomingAppointments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Appointments = new System.Windows.Forms.ComboBox();
            this.toHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Appointments
            // 
            this.Appointments.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Appointments.FormattingEnabled = true;
            this.Appointments.Location = new System.Drawing.Point(385, 234);
            this.Appointments.Name = "Appointments";
            this.Appointments.Size = new System.Drawing.Size(886, 46);
            this.Appointments.TabIndex = 0;
            // 
            // toHome
            // 
            this.toHome.Location = new System.Drawing.Point(0, 0);
            this.toHome.Name = "toHome";
            this.toHome.Size = new System.Drawing.Size(75, 23);
            this.toHome.TabIndex = 17;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 16;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // UpcomingAppointments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleVioletRed;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toHome);
            this.Controls.Add(this.Appointments);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UpcomingAppointments";
            this.Text = "UpcomingAppointments";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox Appointments;
        private System.Windows.Forms.Button toHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}