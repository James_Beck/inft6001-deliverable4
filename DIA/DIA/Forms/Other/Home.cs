﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
            Logout.loggedout();
        }

        private void Login_Click(object sender, EventArgs e)
        {
            string[] arr = { "client", "instructor", "admin" };

            for (int i = 0; i < arr.Count(); i++)
            {
                SQL.selectQuery($"SELECT * FROM {arr[i]}");

                if (SQL.see.HasRows)
                {
                    while (SQL.see.Read())
                    {
                        if (SQL.see[0].ToString() == UsernameField.Text.Trim() && SQL.see[1].ToString() == PasswordField.Text.Trim())
                        {
                            switch(arr[i])
                            {
                                case "client":
                                    //Login information from current client
                                    CurrentClientInfo.Username = SQL.see[0].ToString();
                                    CurrentClientInfo.Fname = SQL.see[2].ToString();
                                    CurrentClientInfo.Lname = SQL.see[3].ToString();
                                    CurrentClientInfo.Email = SQL.see[4].ToString();
                                    CurrentClientInfo.License = SQL.see[5].ToString();
                                    CurrentClientInfo.Hours = (int)SQL.see[6];
                                    break;
                                case "instructor":
                                    //Login information from current instructor
                                    CurrentInstructorInfo.Username = SQL.see[0].ToString();
                                    CurrentInstructorInfo.Fname = SQL.see[2].ToString();
                                    CurrentInstructorInfo.Lname = SQL.see[3].ToString();
                                    CurrentInstructorInfo.Email = SQL.see[4].ToString();
                                    CurrentInstructorInfo.Phone = SQL.see[5].ToString();
                                    CurrentInstructorInfo.Workertype = SQL.see[6].ToString();
                                    CurrentInstructorInfo.deptmng = (bool)SQL.see[7];
                                    break;
                                case "admin":
                                    //Login information from current admin
                                    CurrentAdminInfo.Username = SQL.see[0].ToString();
                                    CurrentAdminInfo.Fname = SQL.see[2].ToString();
                                    CurrentAdminInfo.Lname = SQL.see[3].ToString();
                                    CurrentAdminInfo.Email = SQL.see[4].ToString();
                                    CurrentAdminInfo.Phone = SQL.see[5].ToString();
                                    break;
                            }

                            Hide();
                            UpcomingAppointments inward = new UpcomingAppointments();
                            inward.ShowDialog();
                            Close();
                        }
                    }
                }
            }
        }

        private void toRegister_Click(object sender, EventArgs e)
        {
            Hide();
            Registration register = new Registration();
            register.ShowDialog();
            Close();
        }
    }
}
