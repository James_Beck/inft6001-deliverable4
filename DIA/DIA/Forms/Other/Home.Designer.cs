﻿namespace DIA
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PasswordField = new System.Windows.Forms.TextBox();
            this.toRegister = new System.Windows.Forms.Button();
            this.Login = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UsernameField
            // 
            this.UsernameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.UsernameField.Location = new System.Drawing.Point(254, 253);
            this.UsernameField.Name = "UsernameField";
            this.UsernameField.Size = new System.Drawing.Size(512, 83);
            this.UsernameField.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.label2.Location = new System.Drawing.Point(336, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(341, 76);
            this.label2.TabIndex = 2;
            this.label2.Text = "Username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.label3.Location = new System.Drawing.Point(1056, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(327, 76);
            this.label3.TabIndex = 3;
            this.label3.Text = "Password";
            // 
            // PasswordField
            // 
            this.PasswordField.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.PasswordField.Location = new System.Drawing.Point(970, 253);
            this.PasswordField.Name = "PasswordField";
            this.PasswordField.Size = new System.Drawing.Size(540, 83);
            this.PasswordField.TabIndex = 2;
            // 
            // toRegister
            // 
            this.toRegister.BackColor = System.Drawing.Color.Teal;
            this.toRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.toRegister.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toRegister.Location = new System.Drawing.Point(627, 585);
            this.toRegister.Name = "toRegister";
            this.toRegister.Size = new System.Drawing.Size(527, 115);
            this.toRegister.TabIndex = 4;
            this.toRegister.Text = "Register";
            this.toRegister.UseVisualStyleBackColor = false;
            this.toRegister.Click += new System.EventHandler(this.toRegister_Click);
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.Color.DarkGreen;
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F);
            this.Login.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Login.Location = new System.Drawing.Point(627, 422);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(527, 115);
            this.Login.TabIndex = 3;
            this.Login.Text = "Login";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleVioletRed;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.toRegister);
            this.Controls.Add(this.PasswordField);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UsernameField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Home";
            this.Text = "Home";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UsernameField;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PasswordField;
        private System.Windows.Forms.Button toRegister;
        private System.Windows.Forms.Button Login;
    }
}