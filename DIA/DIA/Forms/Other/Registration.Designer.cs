﻿namespace DIA
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegClient = new System.Windows.Forms.Button();
            this.RegInstructor = new System.Windows.Forms.Button();
            this.RegAdmin = new System.Windows.Forms.Button();
            this.UsernameField = new System.Windows.Forms.TextBox();
            this.PasswordField = new System.Windows.Forms.TextBox();
            this.FNameField = new System.Windows.Forms.TextBox();
            this.LNameField = new System.Windows.Forms.TextBox();
            this.EmailField = new System.Windows.Forms.TextBox();
            this.LabUsername = new System.Windows.Forms.Label();
            this.LabPassword = new System.Windows.Forms.Label();
            this.LabFName = new System.Windows.Forms.Label();
            this.LabLName = new System.Windows.Forms.Label();
            this.LabEmail = new System.Windows.Forms.Label();
            this.LabPhone = new System.Windows.Forms.Label();
            this.PhoneField = new System.Windows.Forms.TextBox();
            this.LabLicense = new System.Windows.Forms.Label();
            this.LabWorkType = new System.Windows.Forms.Label();
            this.ReqPassField = new System.Windows.Forms.TextBox();
            this.LabReqPass = new System.Windows.Forms.Label();
            this.Register = new System.Windows.Forms.Button();
            this.DropLicense = new System.Windows.Forms.ComboBox();
            this.DropWorkType = new System.Windows.Forms.ComboBox();
            this.toLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RegClient
            // 
            this.RegClient.BackColor = System.Drawing.Color.PaleTurquoise;
            this.RegClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.RegClient.Location = new System.Drawing.Point(151, 103);
            this.RegClient.Name = "RegClient";
            this.RegClient.Size = new System.Drawing.Size(476, 155);
            this.RegClient.TabIndex = 0;
            this.RegClient.Text = "Register Client";
            this.RegClient.UseVisualStyleBackColor = false;
            this.RegClient.Click += new System.EventHandler(this.RegClient_Click);
            // 
            // RegInstructor
            // 
            this.RegInstructor.BackColor = System.Drawing.Color.PaleGreen;
            this.RegInstructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.RegInstructor.Location = new System.Drawing.Point(151, 409);
            this.RegInstructor.Name = "RegInstructor";
            this.RegInstructor.Size = new System.Drawing.Size(476, 155);
            this.RegInstructor.TabIndex = 1;
            this.RegInstructor.Text = "Register Instructor";
            this.RegInstructor.UseVisualStyleBackColor = false;
            this.RegInstructor.Click += new System.EventHandler(this.RegInstructor_Click);
            // 
            // RegAdmin
            // 
            this.RegAdmin.BackColor = System.Drawing.Color.LightSalmon;
            this.RegAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.RegAdmin.Location = new System.Drawing.Point(151, 713);
            this.RegAdmin.Name = "RegAdmin";
            this.RegAdmin.Size = new System.Drawing.Size(476, 155);
            this.RegAdmin.TabIndex = 2;
            this.RegAdmin.Text = "Register Admin";
            this.RegAdmin.UseVisualStyleBackColor = false;
            this.RegAdmin.Click += new System.EventHandler(this.RegAdmin_Click);
            // 
            // UsernameField
            // 
            this.UsernameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.UsernameField.Location = new System.Drawing.Point(1063, 103);
            this.UsernameField.Name = "UsernameField";
            this.UsernameField.Size = new System.Drawing.Size(664, 45);
            this.UsernameField.TabIndex = 3;
            // 
            // PasswordField
            // 
            this.PasswordField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.PasswordField.Location = new System.Drawing.Point(1063, 197);
            this.PasswordField.Name = "PasswordField";
            this.PasswordField.Size = new System.Drawing.Size(664, 45);
            this.PasswordField.TabIndex = 4;
            // 
            // FNameField
            // 
            this.FNameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.FNameField.Location = new System.Drawing.Point(1063, 291);
            this.FNameField.Name = "FNameField";
            this.FNameField.Size = new System.Drawing.Size(664, 45);
            this.FNameField.TabIndex = 5;
            // 
            // LNameField
            // 
            this.LNameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LNameField.Location = new System.Drawing.Point(1063, 377);
            this.LNameField.Name = "LNameField";
            this.LNameField.Size = new System.Drawing.Size(664, 45);
            this.LNameField.TabIndex = 6;
            // 
            // EmailField
            // 
            this.EmailField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.EmailField.Location = new System.Drawing.Point(1063, 465);
            this.EmailField.Name = "EmailField";
            this.EmailField.Size = new System.Drawing.Size(664, 45);
            this.EmailField.TabIndex = 7;
            // 
            // LabUsername
            // 
            this.LabUsername.AutoSize = true;
            this.LabUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabUsername.Location = new System.Drawing.Point(767, 103);
            this.LabUsername.Name = "LabUsername";
            this.LabUsername.Size = new System.Drawing.Size(174, 39);
            this.LabUsername.TabIndex = 8;
            this.LabUsername.Text = "Username";
            // 
            // LabPassword
            // 
            this.LabPassword.AutoSize = true;
            this.LabPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabPassword.Location = new System.Drawing.Point(774, 197);
            this.LabPassword.Name = "LabPassword";
            this.LabPassword.Size = new System.Drawing.Size(167, 39);
            this.LabPassword.TabIndex = 9;
            this.LabPassword.Text = "Password";
            // 
            // LabFName
            // 
            this.LabFName.AutoSize = true;
            this.LabFName.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabFName.Location = new System.Drawing.Point(774, 291);
            this.LabFName.Name = "LabFName";
            this.LabFName.Size = new System.Drawing.Size(183, 39);
            this.LabFName.TabIndex = 10;
            this.LabFName.Text = "First Name";
            // 
            // LabLName
            // 
            this.LabLName.AutoSize = true;
            this.LabLName.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabLName.Location = new System.Drawing.Point(774, 377);
            this.LabLName.Name = "LabLName";
            this.LabLName.Size = new System.Drawing.Size(181, 39);
            this.LabLName.TabIndex = 11;
            this.LabLName.Text = "Last Name";
            // 
            // LabEmail
            // 
            this.LabEmail.AutoSize = true;
            this.LabEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabEmail.Location = new System.Drawing.Point(774, 471);
            this.LabEmail.Name = "LabEmail";
            this.LabEmail.Size = new System.Drawing.Size(103, 39);
            this.LabEmail.TabIndex = 12;
            this.LabEmail.Text = "Email";
            // 
            // LabPhone
            // 
            this.LabPhone.AutoSize = true;
            this.LabPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabPhone.Location = new System.Drawing.Point(774, 557);
            this.LabPhone.Name = "LabPhone";
            this.LabPhone.Size = new System.Drawing.Size(116, 39);
            this.LabPhone.TabIndex = 13;
            this.LabPhone.Text = "Phone";
            // 
            // PhoneField
            // 
            this.PhoneField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.PhoneField.Location = new System.Drawing.Point(1063, 557);
            this.PhoneField.Name = "PhoneField";
            this.PhoneField.Size = new System.Drawing.Size(664, 45);
            this.PhoneField.TabIndex = 14;
            // 
            // LabLicense
            // 
            this.LabLicense.AutoSize = true;
            this.LabLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabLicense.Location = new System.Drawing.Point(775, 557);
            this.LabLicense.Name = "LabLicense";
            this.LabLicense.Size = new System.Drawing.Size(135, 39);
            this.LabLicense.TabIndex = 15;
            this.LabLicense.Text = "License";
            // 
            // LabWorkType
            // 
            this.LabWorkType.AutoSize = true;
            this.LabWorkType.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabWorkType.Location = new System.Drawing.Point(773, 653);
            this.LabWorkType.Name = "LabWorkType";
            this.LabWorkType.Size = new System.Drawing.Size(211, 39);
            this.LabWorkType.TabIndex = 19;
            this.LabWorkType.Text = "Worker Type";
            // 
            // ReqPassField
            // 
            this.ReqPassField.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.ReqPassField.Location = new System.Drawing.Point(1064, 817);
            this.ReqPassField.Name = "ReqPassField";
            this.ReqPassField.Size = new System.Drawing.Size(664, 45);
            this.ReqPassField.TabIndex = 22;
            // 
            // LabReqPass
            // 
            this.LabReqPass.AutoSize = true;
            this.LabReqPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.LabReqPass.Location = new System.Drawing.Point(774, 823);
            this.LabReqPass.Name = "LabReqPass";
            this.LabReqPass.Size = new System.Drawing.Size(241, 39);
            this.LabReqPass.TabIndex = 21;
            this.LabReqPass.Text = "Required Pass";
            // 
            // Register
            // 
            this.Register.BackColor = System.Drawing.Color.DarkGreen;
            this.Register.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Register.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Register.Location = new System.Drawing.Point(751, 937);
            this.Register.Name = "Register";
            this.Register.Size = new System.Drawing.Size(688, 58);
            this.Register.TabIndex = 23;
            this.Register.Text = "Register New Acccount";
            this.Register.UseVisualStyleBackColor = false;
            this.Register.Click += new System.EventHandler(this.Register_Click);
            // 
            // DropLicense
            // 
            this.DropLicense.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DropLicense.FormattingEnabled = true;
            this.DropLicense.Location = new System.Drawing.Point(1063, 557);
            this.DropLicense.Name = "DropLicense";
            this.DropLicense.Size = new System.Drawing.Size(665, 46);
            this.DropLicense.TabIndex = 24;
            // 
            // DropWorkType
            // 
            this.DropWorkType.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.DropWorkType.FormattingEnabled = true;
            this.DropWorkType.Location = new System.Drawing.Point(1063, 653);
            this.DropWorkType.Name = "DropWorkType";
            this.DropWorkType.Size = new System.Drawing.Size(665, 46);
            this.DropWorkType.TabIndex = 25;
            // 
            // toLogin
            // 
            this.toLogin.BackColor = System.Drawing.Color.DarkRed;
            this.toLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.toLogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toLogin.Location = new System.Drawing.Point(151, 937);
            this.toLogin.Name = "toLogin";
            this.toLogin.Size = new System.Drawing.Size(476, 58);
            this.toLogin.TabIndex = 26;
            this.toLogin.Text = "Back";
            this.toLogin.UseVisualStyleBackColor = false;
            this.toLogin.Click += new System.EventHandler(this.toLogin_Click);
            // 
            // Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleVioletRed;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.toLogin);
            this.Controls.Add(this.DropWorkType);
            this.Controls.Add(this.DropLicense);
            this.Controls.Add(this.Register);
            this.Controls.Add(this.ReqPassField);
            this.Controls.Add(this.LabReqPass);
            this.Controls.Add(this.LabWorkType);
            this.Controls.Add(this.LabLicense);
            this.Controls.Add(this.PhoneField);
            this.Controls.Add(this.LabPhone);
            this.Controls.Add(this.LabEmail);
            this.Controls.Add(this.LabLName);
            this.Controls.Add(this.LabFName);
            this.Controls.Add(this.LabPassword);
            this.Controls.Add(this.LabUsername);
            this.Controls.Add(this.EmailField);
            this.Controls.Add(this.LNameField);
            this.Controls.Add(this.FNameField);
            this.Controls.Add(this.PasswordField);
            this.Controls.Add(this.UsernameField);
            this.Controls.Add(this.RegAdmin);
            this.Controls.Add(this.RegInstructor);
            this.Controls.Add(this.RegClient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Registration";
            this.Text = "Registration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button RegClient;
        private System.Windows.Forms.Button RegInstructor;
        private System.Windows.Forms.Button RegAdmin;
        private System.Windows.Forms.TextBox UsernameField;
        private System.Windows.Forms.TextBox PasswordField;
        private System.Windows.Forms.TextBox FNameField;
        private System.Windows.Forms.TextBox LNameField;
        private System.Windows.Forms.TextBox EmailField;
        private System.Windows.Forms.Label LabUsername;
        private System.Windows.Forms.Label LabPassword;
        private System.Windows.Forms.Label LabFName;
        private System.Windows.Forms.Label LabLName;
        private System.Windows.Forms.Label LabEmail;
        private System.Windows.Forms.Label LabPhone;
        private System.Windows.Forms.TextBox PhoneField;
        private System.Windows.Forms.Label LabLicense;
        private System.Windows.Forms.Label LabWorkType;
        private System.Windows.Forms.TextBox ReqPassField;
        private System.Windows.Forms.Label LabReqPass;
        private System.Windows.Forms.Button Register;
        private System.Windows.Forms.ComboBox DropLicense;
        private System.Windows.Forms.ComboBox DropWorkType;
        private System.Windows.Forms.Button toLogin;
    }
}