﻿namespace DIA
{
    partial class Statistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NumClients = new System.Windows.Forms.Label();
            this.NumInst = new System.Windows.Forms.Label();
            this.NumAdmins = new System.Windows.Forms.Label();
            this.FullLicenses = new System.Windows.Forms.Label();
            this.LearnerLicenses = new System.Windows.Forms.Label();
            this.CertificatesAwarded = new System.Windows.Forms.Label();
            this.DepartmentMngs = new System.Windows.Forms.Label();
            this.TotalCars = new System.Windows.Forms.Label();
            this.CarsInMain = new System.Windows.Forms.Label();
            this.CasEmps = new System.Windows.Forms.Label();
            this.PermEmps = new System.Windows.Forms.Label();
            this.billspaid = new System.Windows.Forms.Label();
            this.billsunpaid = new System.Windows.Forms.Label();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F);
            this.label1.Location = new System.Drawing.Point(437, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(969, 120);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Statistics";
            // 
            // NumClients
            // 
            this.NumClients.AutoSize = true;
            this.NumClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.NumClients.Location = new System.Drawing.Point(438, 343);
            this.NumClients.Name = "NumClients";
            this.NumClients.Size = new System.Drawing.Size(73, 31);
            this.NumClients.TabIndex = 1;
            this.NumClients.Text = "Here";
            // 
            // NumInst
            // 
            this.NumInst.AutoSize = true;
            this.NumInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.NumInst.Location = new System.Drawing.Point(438, 403);
            this.NumInst.Name = "NumInst";
            this.NumInst.Size = new System.Drawing.Size(73, 31);
            this.NumInst.TabIndex = 2;
            this.NumInst.Text = "Here";
            // 
            // NumAdmins
            // 
            this.NumAdmins.AutoSize = true;
            this.NumAdmins.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.NumAdmins.Location = new System.Drawing.Point(438, 466);
            this.NumAdmins.Name = "NumAdmins";
            this.NumAdmins.Size = new System.Drawing.Size(73, 31);
            this.NumAdmins.TabIndex = 3;
            this.NumAdmins.Text = "Here";
            // 
            // FullLicenses
            // 
            this.FullLicenses.AutoSize = true;
            this.FullLicenses.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.FullLicenses.Location = new System.Drawing.Point(438, 531);
            this.FullLicenses.Name = "FullLicenses";
            this.FullLicenses.Size = new System.Drawing.Size(73, 31);
            this.FullLicenses.TabIndex = 4;
            this.FullLicenses.Text = "Here";
            // 
            // LearnerLicenses
            // 
            this.LearnerLicenses.AutoSize = true;
            this.LearnerLicenses.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.LearnerLicenses.Location = new System.Drawing.Point(438, 592);
            this.LearnerLicenses.Name = "LearnerLicenses";
            this.LearnerLicenses.Size = new System.Drawing.Size(73, 31);
            this.LearnerLicenses.TabIndex = 5;
            this.LearnerLicenses.Text = "Here";
            // 
            // CertificatesAwarded
            // 
            this.CertificatesAwarded.AutoSize = true;
            this.CertificatesAwarded.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CertificatesAwarded.Location = new System.Drawing.Point(944, 531);
            this.CertificatesAwarded.Name = "CertificatesAwarded";
            this.CertificatesAwarded.Size = new System.Drawing.Size(73, 31);
            this.CertificatesAwarded.TabIndex = 6;
            this.CertificatesAwarded.Text = "Here";
            // 
            // DepartmentMngs
            // 
            this.DepartmentMngs.AutoSize = true;
            this.DepartmentMngs.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.DepartmentMngs.Location = new System.Drawing.Point(944, 466);
            this.DepartmentMngs.Name = "DepartmentMngs";
            this.DepartmentMngs.Size = new System.Drawing.Size(73, 31);
            this.DepartmentMngs.TabIndex = 7;
            this.DepartmentMngs.Text = "Here";
            // 
            // TotalCars
            // 
            this.TotalCars.AutoSize = true;
            this.TotalCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TotalCars.Location = new System.Drawing.Point(944, 592);
            this.TotalCars.Name = "TotalCars";
            this.TotalCars.Size = new System.Drawing.Size(73, 31);
            this.TotalCars.TabIndex = 11;
            this.TotalCars.Text = "Here";
            // 
            // CarsInMain
            // 
            this.CarsInMain.AutoSize = true;
            this.CarsInMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CarsInMain.Location = new System.Drawing.Point(944, 662);
            this.CarsInMain.Name = "CarsInMain";
            this.CarsInMain.Size = new System.Drawing.Size(73, 31);
            this.CarsInMain.TabIndex = 12;
            this.CarsInMain.Text = "Here";
            // 
            // CasEmps
            // 
            this.CasEmps.AutoSize = true;
            this.CasEmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CasEmps.Location = new System.Drawing.Point(944, 343);
            this.CasEmps.Name = "CasEmps";
            this.CasEmps.Size = new System.Drawing.Size(73, 31);
            this.CasEmps.TabIndex = 13;
            this.CasEmps.Text = "Here";
            // 
            // PermEmps
            // 
            this.PermEmps.AutoSize = true;
            this.PermEmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.PermEmps.Location = new System.Drawing.Point(944, 403);
            this.PermEmps.Name = "PermEmps";
            this.PermEmps.Size = new System.Drawing.Size(73, 31);
            this.PermEmps.TabIndex = 14;
            this.PermEmps.Text = "Here";
            // 
            // billspaid
            // 
            this.billspaid.AutoSize = true;
            this.billspaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.billspaid.Location = new System.Drawing.Point(438, 662);
            this.billspaid.Name = "billspaid";
            this.billspaid.Size = new System.Drawing.Size(73, 31);
            this.billspaid.TabIndex = 15;
            this.billspaid.Text = "Here";
            // 
            // billsunpaid
            // 
            this.billsunpaid.AutoSize = true;
            this.billsunpaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.billsunpaid.Location = new System.Drawing.Point(438, 732);
            this.billsunpaid.Name = "billsunpaid";
            this.billsunpaid.Size = new System.Drawing.Size(73, 31);
            this.billsunpaid.TabIndex = 16;
            this.billsunpaid.Text = "Here";
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(-1, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 17;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // Statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.billsunpaid);
            this.Controls.Add(this.billspaid);
            this.Controls.Add(this.PermEmps);
            this.Controls.Add(this.CasEmps);
            this.Controls.Add(this.CarsInMain);
            this.Controls.Add(this.TotalCars);
            this.Controls.Add(this.DepartmentMngs);
            this.Controls.Add(this.CertificatesAwarded);
            this.Controls.Add(this.LearnerLicenses);
            this.Controls.Add(this.FullLicenses);
            this.Controls.Add(this.NumAdmins);
            this.Controls.Add(this.NumInst);
            this.Controls.Add(this.NumClients);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Statistics";
            this.Text = "Statistics";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NumClients;
        private System.Windows.Forms.Label NumInst;
        private System.Windows.Forms.Label NumAdmins;
        private System.Windows.Forms.Label FullLicenses;
        private System.Windows.Forms.Label LearnerLicenses;
        private System.Windows.Forms.Label CertificatesAwarded;
        private System.Windows.Forms.Label DepartmentMngs;
        private System.Windows.Forms.Label TotalCars;
        private System.Windows.Forms.Label CarsInMain;
        private System.Windows.Forms.Label CasEmps;
        private System.Windows.Forms.Label PermEmps;
        private System.Windows.Forms.Label billspaid;
        private System.Windows.Forms.Label billsunpaid;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}