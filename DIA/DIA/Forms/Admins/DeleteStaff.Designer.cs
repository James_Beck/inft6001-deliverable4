﻿namespace DIA
{
    partial class DeleteStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabExplain = new System.Windows.Forms.Label();
            this.StaffList = new System.Windows.Forms.ComboBox();
            this.Confirm = new System.Windows.Forms.Button();
            this.staffusername = new System.Windows.Forms.ComboBox();
            this.toAdHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // LabExplain
            // 
            this.LabExplain.AutoSize = true;
            this.LabExplain.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.LabExplain.Location = new System.Drawing.Point(451, 172);
            this.LabExplain.Name = "LabExplain";
            this.LabExplain.Size = new System.Drawing.Size(763, 63);
            this.LabExplain.TabIndex = 0;
            this.LabExplain.Text = "Select Staff Member To Delete";
            // 
            // StaffList
            // 
            this.StaffList.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.StaffList.FormattingEnabled = true;
            this.StaffList.Location = new System.Drawing.Point(619, 271);
            this.StaffList.Name = "StaffList";
            this.StaffList.Size = new System.Drawing.Size(595, 46);
            this.StaffList.TabIndex = 1;
            // 
            // Confirm
            // 
            this.Confirm.BackColor = System.Drawing.Color.DarkRed;
            this.Confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Confirm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Confirm.Location = new System.Drawing.Point(462, 352);
            this.Confirm.Name = "Confirm";
            this.Confirm.Size = new System.Drawing.Size(752, 57);
            this.Confirm.TabIndex = 2;
            this.Confirm.Text = "Confirm Delete";
            this.Confirm.UseVisualStyleBackColor = false;
            this.Confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // staffusername
            // 
            this.staffusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.staffusername.FormattingEnabled = true;
            this.staffusername.Location = new System.Drawing.Point(462, 271);
            this.staffusername.Name = "staffusername";
            this.staffusername.Size = new System.Drawing.Size(141, 46);
            this.staffusername.TabIndex = 5;
            // 
            // toAdHome
            // 
            this.toAdHome.Location = new System.Drawing.Point(0, 0);
            this.toAdHome.Name = "toAdHome";
            this.toAdHome.Size = new System.Drawing.Size(75, 23);
            this.toAdHome.TabIndex = 14;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 13;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // DeleteStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toAdHome);
            this.Controls.Add(this.staffusername);
            this.Controls.Add(this.Confirm);
            this.Controls.Add(this.StaffList);
            this.Controls.Add(this.LabExplain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DeleteStaff";
            this.Text = "DeleteStaff";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabExplain;
        private System.Windows.Forms.ComboBox StaffList;
        private System.Windows.Forms.Button Confirm;
        private System.Windows.Forms.ComboBox staffusername;
        private System.Windows.Forms.Button toAdHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}