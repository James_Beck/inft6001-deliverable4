﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class Statistics : Form
    {
        public Statistics()
        {
            InitializeComponent();
            fillmenu();
            countaccounts();
            licensesinfo();
            certificateinfo();
            carinfo();
            employeeinfo();
            moneyinfo();
        }

        private void countaccounts()
        {
            //number of clients
            SQL.selectQuery("SELECT COUNT(*) FROM client");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    NumClients.Text = $"Number of Clients: {SQL.see[0].ToString()}";
                }
            }

            //number of instructors
            SQL.selectQuery("SELECT COUNT(*) FROM instructor");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    NumInst.Text = $"Number of Instructors: {SQL.see[0].ToString()}";
                }
            }

            //number of admins
            SQL.selectQuery("SELECT COUNT(*) FROM admin");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    NumAdmins.Text = $"Number of Admins: {SQL.see[0].ToString()}";
                }
            }
        }

        private void licensesinfo()
        {
            //number of full licensed clients
            SQL.selectQuery("SELECT COUNT(license) FROM client WHERE license LIKE 'Full'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    FullLicenses.Text = $"Full License Clients: {SQL.see[0].ToString()}";
                }
            }

            //number of learner licensed clients
            SQL.selectQuery("SELECT COUNT(license) FROM client WHERE license LIKE 'Learner'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    LearnerLicenses.Text = $"Learner License Clients: {SQL.see[0].ToString()}";
                }
            }
        }

        private void certificateinfo()
        {
            //number of certificates earned
            SQL.selectQuery("SELECT COUNT(*) FROM client c, bill b WHERE c.clhours LIKE 5 AND c.license LIKE 'Learner' AND b.paid LIKE 1 OR c.clhours LIKE 2 AND c.license LIKE 'Full' AND b.paid LIKE 1");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    CertificatesAwarded.Text = $"Awarded Certificates: {SQL.see[0].ToString()}";
                }
            }
        }

        private void carinfo()
        {
            //number of cars
            SQL.selectQuery("SELECT COUNT(*) FROM vehicle");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    TotalCars.Text = $"Total Cars: {SQL.see[0].ToString()}";
                }
            }

            //number of cars in maintenance
            SQL.selectQuery("SELECT COUNT(*) FROM maintenance");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    CarsInMain.Text = $"Cars in Maintenance: {SQL.see[0].ToString()}";
                }
            }
        }

        private void employeeinfo()
        {
            //casual employees
            SQL.selectQuery("SELECT COUNT(*) FROM instructor WHERE workertype LIKE 'casual'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    CasEmps.Text = $"Casual Employees: {SQL.see[0].ToString()}";
                }
            }

            //permanent employees
            SQL.selectQuery("SELECT COUNT(*) FROM instructor WHERE workertype LIKE 'permanent'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    PermEmps.Text = $"Permanent Employees: {SQL.see[0].ToString()}";
                }
            }

            //casual employees
            SQL.selectQuery("SELECT COUNT(*) FROM instructor WHERE deptmng = 1");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    DepartmentMngs.Text = $"Instructor Managers: {SQL.see[0].ToString()}";
                }
            }
        }

        private void moneyinfo()
        {
            //money paid from bills
            SQL.selectQuery("SELECT SUM(amount) FROM bill WHERE paid = 1");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    billspaid.Text = $"Bill money received: ${SQL.see[0].ToString()}";
                }
            }

            //money owed from bills
            SQL.selectQuery("SELECT SUM(amount) FROM bill WHERE paid = 0");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    billsunpaid.Text = $"Bill money credited: ${SQL.see[0].ToString()}";
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    AssignCar cars = new AssignCar();
                    cars.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    ConfirmUnavailability timeoff = new ConfirmUnavailability();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    DeleteStaff delete = new DeleteStaff();
                    delete.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Statistics stats = new Statistics();
                    stats.ShowDialog();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("All Appointments");
            transmenu.Add("Assign Cars");
            transmenu.Add("Time Off Requests");
            transmenu.Add("Delete Staff Account");
            transmenu.Add("Company Statistics");
            transmenu.Add("--------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
