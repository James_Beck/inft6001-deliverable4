﻿namespace DIA
{
    partial class ConfirmUnavailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Requests = new System.Windows.Forms.ComboBox();
            this.Allow = new System.Windows.Forms.Button();
            this.Deny = new System.Windows.Forms.Button();
            this.ConfirmedChange = new System.Windows.Forms.Label();
            this.toAdHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Requests
            // 
            this.Requests.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Requests.FormattingEnabled = true;
            this.Requests.Location = new System.Drawing.Point(497, 346);
            this.Requests.Name = "Requests";
            this.Requests.Size = new System.Drawing.Size(731, 46);
            this.Requests.TabIndex = 0;
            // 
            // Allow
            // 
            this.Allow.BackColor = System.Drawing.Color.DarkGreen;
            this.Allow.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Allow.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Allow.Location = new System.Drawing.Point(497, 609);
            this.Allow.Name = "Allow";
            this.Allow.Size = new System.Drawing.Size(353, 117);
            this.Allow.TabIndex = 1;
            this.Allow.Text = "Allow Timeoff";
            this.Allow.UseVisualStyleBackColor = false;
            this.Allow.Click += new System.EventHandler(this.Allow_Click);
            // 
            // Deny
            // 
            this.Deny.BackColor = System.Drawing.Color.DarkRed;
            this.Deny.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Deny.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Deny.Location = new System.Drawing.Point(875, 609);
            this.Deny.Name = "Deny";
            this.Deny.Size = new System.Drawing.Size(353, 117);
            this.Deny.TabIndex = 2;
            this.Deny.Text = "Deny Timeoff";
            this.Deny.UseVisualStyleBackColor = false;
            this.Deny.Click += new System.EventHandler(this.Deny_Click);
            // 
            // ConfirmedChange
            // 
            this.ConfirmedChange.AutoSize = true;
            this.ConfirmedChange.Location = new System.Drawing.Point(12, 94);
            this.ConfirmedChange.Name = "ConfirmedChange";
            this.ConfirmedChange.Size = new System.Drawing.Size(0, 13);
            this.ConfirmedChange.TabIndex = 3;
            // 
            // toAdHome
            // 
            this.toAdHome.Location = new System.Drawing.Point(0, 0);
            this.toAdHome.Name = "toAdHome";
            this.toAdHome.Size = new System.Drawing.Size(75, 23);
            this.toAdHome.TabIndex = 14;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 13;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // ConfirmUnavailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toAdHome);
            this.Controls.Add(this.ConfirmedChange);
            this.Controls.Add(this.Deny);
            this.Controls.Add(this.Allow);
            this.Controls.Add(this.Requests);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConfirmUnavailability";
            this.Text = "ConfirmUnavailability";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Requests;
        private System.Windows.Forms.Button Allow;
        private System.Windows.Forms.Button Deny;
        private System.Windows.Forms.Label ConfirmedChange;
        private System.Windows.Forms.Button toAdHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
    }
}