﻿namespace DIA
{
    partial class AssignCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UnassignedCars = new System.Windows.Forms.ComboBox();
            this.UnassignedInstructors = new System.Windows.Forms.ComboBox();
            this.Assigned = new System.Windows.Forms.ComboBox();
            this.Maintenance = new System.Windows.Forms.Button();
            this.Confirm = new System.Windows.Forms.CheckBox();
            this.Confirm2 = new System.Windows.Forms.CheckBox();
            this.Back = new System.Windows.Forms.Button();
            this.inmaintenance = new System.Windows.Forms.ComboBox();
            this.CarnInst = new System.Windows.Forms.Button();
            this.RemoveAss = new System.Windows.Forms.Button();
            this.toAdHome = new System.Windows.Forms.Button();
            this.TransitionalMenu = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UnassignedCars
            // 
            this.UnassignedCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.UnassignedCars.FormattingEnabled = true;
            this.UnassignedCars.Location = new System.Drawing.Point(174, 240);
            this.UnassignedCars.Name = "UnassignedCars";
            this.UnassignedCars.Size = new System.Drawing.Size(430, 46);
            this.UnassignedCars.TabIndex = 0;
            // 
            // UnassignedInstructors
            // 
            this.UnassignedInstructors.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.UnassignedInstructors.FormattingEnabled = true;
            this.UnassignedInstructors.Location = new System.Drawing.Point(174, 394);
            this.UnassignedInstructors.Name = "UnassignedInstructors";
            this.UnassignedInstructors.Size = new System.Drawing.Size(568, 46);
            this.UnassignedInstructors.TabIndex = 1;
            // 
            // Assigned
            // 
            this.Assigned.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Assigned.FormattingEnabled = true;
            this.Assigned.Location = new System.Drawing.Point(174, 558);
            this.Assigned.Name = "Assigned";
            this.Assigned.Size = new System.Drawing.Size(535, 46);
            this.Assigned.TabIndex = 2;
            // 
            // Maintenance
            // 
            this.Maintenance.BackColor = System.Drawing.Color.DarkBlue;
            this.Maintenance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Maintenance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Maintenance.Location = new System.Drawing.Point(941, 393);
            this.Maintenance.Name = "Maintenance";
            this.Maintenance.Size = new System.Drawing.Size(521, 44);
            this.Maintenance.TabIndex = 3;
            this.Maintenance.Text = "Assign To Maintenance";
            this.Maintenance.UseVisualStyleBackColor = false;
            this.Maintenance.Click += new System.EventHandler(this.Maintenance_Click);
            // 
            // Confirm
            // 
            this.Confirm.AutoSize = true;
            this.Confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Confirm.Location = new System.Drawing.Point(1499, 402);
            this.Confirm.Name = "Confirm";
            this.Confirm.Size = new System.Drawing.Size(128, 35);
            this.Confirm.TabIndex = 4;
            this.Confirm.Text = "Confirm";
            this.Confirm.UseVisualStyleBackColor = true;
            // 
            // Confirm2
            // 
            this.Confirm2.AutoSize = true;
            this.Confirm2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Confirm2.Location = new System.Drawing.Point(1499, 606);
            this.Confirm2.Name = "Confirm2";
            this.Confirm2.Size = new System.Drawing.Size(128, 35);
            this.Confirm2.TabIndex = 6;
            this.Confirm2.Text = "Confirm";
            this.Confirm2.UseVisualStyleBackColor = true;
            // 
            // Back
            // 
            this.Back.BackColor = System.Drawing.Color.DarkBlue;
            this.Back.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.Back.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Back.Location = new System.Drawing.Point(941, 600);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(521, 44);
            this.Back.TabIndex = 5;
            this.Back.Text = "Back From Maintenance";
            this.Back.UseVisualStyleBackColor = false;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // inmaintenance
            // 
            this.inmaintenance.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.inmaintenance.FormattingEnabled = true;
            this.inmaintenance.Location = new System.Drawing.Point(174, 723);
            this.inmaintenance.Name = "inmaintenance";
            this.inmaintenance.Size = new System.Drawing.Size(521, 46);
            this.inmaintenance.TabIndex = 7;
            // 
            // CarnInst
            // 
            this.CarnInst.BackColor = System.Drawing.Color.DarkBlue;
            this.CarnInst.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.CarnInst.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CarnInst.Location = new System.Drawing.Point(941, 465);
            this.CarnInst.Name = "CarnInst";
            this.CarnInst.Size = new System.Drawing.Size(521, 44);
            this.CarnInst.TabIndex = 8;
            this.CarnInst.Text = "Assign Car/Instructor";
            this.CarnInst.UseVisualStyleBackColor = false;
            this.CarnInst.Click += new System.EventHandler(this.CarnInst_Click);
            // 
            // RemoveAss
            // 
            this.RemoveAss.BackColor = System.Drawing.Color.DarkBlue;
            this.RemoveAss.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.RemoveAss.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.RemoveAss.Location = new System.Drawing.Point(941, 533);
            this.RemoveAss.Name = "RemoveAss";
            this.RemoveAss.Size = new System.Drawing.Size(521, 44);
            this.RemoveAss.TabIndex = 9;
            this.RemoveAss.Text = "Unassign Car/Instructor";
            this.RemoveAss.UseVisualStyleBackColor = false;
            this.RemoveAss.Click += new System.EventHandler(this.RemoveAss_Click);
            // 
            // toAdHome
            // 
            this.toAdHome.Location = new System.Drawing.Point(0, 0);
            this.toAdHome.Name = "toAdHome";
            this.toAdHome.Size = new System.Drawing.Size(75, 23);
            this.toAdHome.TabIndex = 14;
            // 
            // TransitionalMenu
            // 
            this.TransitionalMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TransitionalMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.TransitionalMenu.FormattingEnabled = true;
            this.TransitionalMenu.Location = new System.Drawing.Point(0, 0);
            this.TransitionalMenu.Name = "TransitionalMenu";
            this.TransitionalMenu.Size = new System.Drawing.Size(380, 39);
            this.TransitionalMenu.TabIndex = 13;
            this.TransitionalMenu.SelectedIndexChanged += new System.EventHandler(this.TransitionalMenu_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.label1.Location = new System.Drawing.Point(171, 657);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(524, 63);
            this.label1.TabIndex = 15;
            this.label1.Text = "Cars In Maintenance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.label2.Location = new System.Drawing.Point(171, 492);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(538, 63);
            this.label2.TabIndex = 16;
            this.label2.Text = "Car/Instructor Pairing";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.label7.Location = new System.Drawing.Point(163, 328);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(579, 63);
            this.label7.TabIndex = 21;
            this.label7.Text = "Unassigned Instructors";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.label8.Location = new System.Drawing.Point(163, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(441, 63);
            this.label8.TabIndex = 22;
            this.label8.Text = "Unassigned Cars";
            // 
            // AssignCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TransitionalMenu);
            this.Controls.Add(this.toAdHome);
            this.Controls.Add(this.RemoveAss);
            this.Controls.Add(this.CarnInst);
            this.Controls.Add(this.inmaintenance);
            this.Controls.Add(this.Confirm2);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.Confirm);
            this.Controls.Add(this.Maintenance);
            this.Controls.Add(this.Assigned);
            this.Controls.Add(this.UnassignedInstructors);
            this.Controls.Add(this.UnassignedCars);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AssignCar";
            this.Text = "AssignCar";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox UnassignedCars;
        private System.Windows.Forms.ComboBox UnassignedInstructors;
        private System.Windows.Forms.ComboBox Assigned;
        private System.Windows.Forms.Button Maintenance;
        private System.Windows.Forms.CheckBox Confirm;
        private System.Windows.Forms.CheckBox Confirm2;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.ComboBox inmaintenance;
        private System.Windows.Forms.Button CarnInst;
        private System.Windows.Forms.Button RemoveAss;
        private System.Windows.Forms.Button toAdHome;
        private System.Windows.Forms.ComboBox TransitionalMenu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}