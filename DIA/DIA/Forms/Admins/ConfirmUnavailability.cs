﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class ConfirmUnavailability : Form
    {
        //to use as information for where statements
        public List<string> timerange = new List<string>();

        public ConfirmUnavailability()
        {
            InitializeComponent();
            presetinfo();
            fillmenu();
        }

        //grants time off
        private void Allow_Click(object sender, EventArgs e)
        {
            //prevents a no-select on the Requests combobox
            if (Requests.SelectedItem == null)
            {
                MessageBox.Show("Nothing was selected");
                return;
            }

            SQL.executeQuery($"UPDATE timeoff SET confirmed = 1 WHERE requesteddatetime LIKE '{timerange[Requests.Items.IndexOf(Requests.SelectedItem)]}'");

            SQL.selectQuery($"SELECT * FROM timeoff WHERE requesteddatetime LIKE '{timerange[Requests.Items.IndexOf(Requests.SelectedItem)]}'");
            
            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    ConfirmedChange.Text = $"{SQL.see[0]} - {SQL.see[1]} {SQL.see[2]} - {SQL.see[3]}";
                }
            }

            presetinfo();
        }

        //deletes request doesn't grant time off
        private void Deny_Click(object sender, EventArgs e)
        {
            //prevents a no-select on the Requests combobox
            if (Requests.SelectedItem == null)
            {
                MessageBox.Show("Nothing was selected");
                return;
            }

            SQL.executeQuery($"DELETE FROM timeoff WHERE requesteddatetime LIKE '{timerange[Requests.Items.IndexOf(Requests.SelectedItem)]}'");

            SQL.selectQuery($"SELECT * FROM timeoff WHERE requesteddatetime LIKE '{timerange[Requests.Items.IndexOf(Requests.SelectedItem)]}'");

            if (!SQL.see.HasRows)
            {
                MessageBox.Show("The row has been deleted");
            }

            ConfirmedChange.Text = "Row deleted, request denied";
            presetinfo();
        }

        private void presetinfo()
        {
            Requests.Text = null;
            Requests.Items.Clear();

            //populates combobox and list (at same rate and index)
            SQL.selectQuery("SELECT * FROM timeoff");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    Requests.Items.Add($"{SQL.see[0]} - {SQL.see[1]} {SQL.see[2]}");
                    timerange.Add(SQL.see[0].ToString());
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    AssignCar cars = new AssignCar();
                    cars.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    ConfirmUnavailability timeoff = new ConfirmUnavailability();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    DeleteStaff delete = new DeleteStaff();
                    delete.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Statistics stats = new Statistics();
                    stats.ShowDialog();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("All Appointments");
            transmenu.Add("Assign Cars");
            transmenu.Add("Time Off Requests");
            transmenu.Add("Delete Staff Account");
            transmenu.Add("Company Statistics");
            transmenu.Add("--------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
