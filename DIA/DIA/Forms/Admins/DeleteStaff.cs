﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class DeleteStaff : Form
    {
        public static string selectedfname { get; set; }
        public static string selectedlname { get; set; }
        public static bool current { get; set; }

        public DeleteStaff()
        {
            InitializeComponent();
            fillwithstaff();
            fillmenu();
        }        

        private void Confirm_Click(object sender, EventArgs e)
        {
            //set values for select statements
            SQL.selectQuery($"SELECT * FROM instructor WHERE inuser LIKE '{staffusername.SelectedItem}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    selectedfname = SQL.see[2].ToString();
                    selectedlname = SQL.see[3].ToString();
                }
            }

            //check to see if instructor or admin
            if (selectedfname != "")
            {
                selectedisisntructor();
            }
            else
            {
                selectedisadmin();
            }
        }

        private void selectedisisntructor()
        {
            //update appointments table
            SQL.executeQuery($"UPDATE appointment SET infname = 'Unassigned', inlname = 'Unassigned' WHERE infname LIKE '{selectedfname}' AND inlname LIKE '{selectedlname}'");

            //delete the account
            SQL.executeQuery($"DELETE FROM instructor WHERE inuser LIKE '{staffusername.SelectedItem}'");

            //check deletion
            SQL.selectQuery($"SELECT * FROM instructor WHERE inuser LIKE '{staffusername.SelectedItem}'");

            if (!SQL.see.HasRows)
            {
                MessageBox.Show($"Staff account {staffusername.SelectedItem} deleted");
                return;
            }
        }

        private void selectedisadmin()
        {
            //set values for select statements
            SQL.selectQuery($"SELECT * FROM admin WHERE aduser LIKE '{staffusername.SelectedItem}'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    selectedfname = SQL.see[2].ToString();
                    selectedlname = SQL.see[3].ToString();
                }
            }

            //logs out if deleted account is currently logged in
            if (selectedfname == CurrentAdminInfo.Fname && selectedlname == CurrentAdminInfo.Lname)
            {
                Logout.loggedout();
                current = true;
            }

            //delete the account
            SQL.executeQuery($"DELETE FROM admin WHERE aduser LIKE '{staffusername.SelectedItem}'");

            //check deletion
            SQL.selectQuery($"SELECT * FROM admin WHERE aduser LIKE '{staffusername.SelectedItem}'");

            if (!SQL.see.HasRows)
            {
                MessageBox.Show($"Delete has succeeded");
            }

            if(current)
            {
                Hide();
                Home loggedout = new Home();
                Close();
            }
        }

        private void fillwithstaff()
        {
            //clear contents from combobox
            staffusername.Items.Clear();
            staffusername.Text = null;
            StaffList.Items.Clear();
            StaffList.Text = null;

            //get the information for the instructors
            SQL.selectQuery($"SELECT * FROM instructor");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    staffusername.Items.Add(SQL.see[0]);
                    StaffList.Items.Add($"{SQL.see[0]} - {SQL.see[2]} {SQL.see[3]}");
                }
            }

            staffusername.Items.Add("---");
            StaffList.Items.Add("Admin Accounts Below");

            //get the information for the admins
            SQL.selectQuery("SELECT * FROM admin");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    staffusername.Items.Add(SQL.see[0]);
                    StaffList.Items.Add($"{SQL.see[0]} - {SQL.see[2]} {SQL.see[3]}");
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    AssignCar cars = new AssignCar();
                    cars.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    ConfirmUnavailability timeoff = new ConfirmUnavailability();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    DeleteStaff delete = new DeleteStaff();
                    delete.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Statistics stats = new Statistics();
                    stats.ShowDialog();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("All Appointments");
            transmenu.Add("Assign Cars");
            transmenu.Add("Time Off Requests");
            transmenu.Add("Delete Staff Account");
            transmenu.Add("Company Statistics");
            transmenu.Add("--------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}

