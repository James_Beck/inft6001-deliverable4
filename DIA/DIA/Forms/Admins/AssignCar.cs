﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DIA
{
    public partial class AssignCar : Form
    {
        public AssignCar()
        {
            InitializeComponent();

            unassignedtable();
            unassignedinstructors();
            assigned();
            tomaintenance();
            fillmenu();
        }

        private void Maintenance_Click(object sender, EventArgs e)
        {
            if (!Confirm.Checked || UnassignedCars.SelectedItem == null)
            {
                MessageBox.Show("This requires confirmation from the box below and a selected value from the combobox");
                return;
            }

            //sends the car to maintenance
            SQL.executeQuery($"INSERT INTO maintenance VALUES ('{UnassignedCars.SelectedItem}')");

            //updates the vehicle table
            SQL.executeQuery($"UPDATE vehicle SET assigned = 'maintenance' WHERE plate LIKE '{UnassignedCars.SelectedItem}'");
            MessageBox.Show($"{UnassignedCars.SelectedItem} has been sent away for maintenance");

            unassignedtable();
            tomaintenance();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            if (!Confirm2.Checked || inmaintenance.SelectedItem == null)
            {
                MessageBox.Show("This requires confirmation from the box below and a selected value from the combobox");
                return;
            }

            //updates the vehicle table
            SQL.executeQuery($"UPDATE vehicle SET assigned = 'blank' WHERE plate LIKE '{inmaintenance.SelectedItem}'");

            //deletes entry in maintenance table
            SQL.executeQuery($"DELETE FROM maintenance WHERE plate = '{inmaintenance.SelectedItem}'");
            MessageBox.Show($"{inmaintenance.SelectedItem} is back from maintenance and is now unassigned");

            unassignedtable();
            tomaintenance();
        }

        private void CarnInst_Click(object sender, EventArgs e)
        {
            if (UnassignedCars.SelectedItem == null || UnassignedInstructors == null)
            {
                MessageBox.Show("You need to select a value from the car and instructor comboboxes");
                return;
            }

            List<string> username = new List<string>();
            
            SQL.selectQuery($"SELECT inuser FROM instructor");

            if (SQL.see.HasRows)
            {                
                while (SQL.see.Read())
                {
                    username.Add(SQL.see[0].ToString());
                }
            }
            
            foreach (string x in username)
            {
                if (UnassignedInstructors.SelectedItem.ToString().Contains(x))
                {
                    //assigns a car to an instructor
                    SQL.executeQuery($"UPDATE vehicle SET assigned = '{x}' WHERE plate LIKE '{UnassignedCars.SelectedItem}'");
                    MessageBox.Show($"{x} is assigned car {UnassignedCars.SelectedItem}");
                }
            }            

            unassignedtable();
            unassignedinstructors();
            assigned();            
        }

        private void RemoveAss_Click(object sender, EventArgs e)
        {
            if (Assigned.SelectedItem == null)
            {
                MessageBox.Show("You need to select a value from the assigned combobox");
                return;
            }

            List<string> username = new List<string>();

            SQL.selectQuery($"SELECT inuser FROM instructor");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    username.Add(SQL.see[0].ToString());
                }
            }

            foreach (string x in username)
            {
                if (Assigned.SelectedItem.ToString().Contains(x))
                {
                    //unassigns a car to an instructor
                    SQL.executeQuery($"UPDATE vehicle SET assigned = 'blank' WHERE assigned IN (SELECT inuser FROM instructor WHERE inuser LIKE '{x}')");
                    MessageBox.Show($"Car/Instructor pair {x} now unassigned");
                }
            }

            unassignedtable();
            unassignedinstructors();
            assigned();
        }

        private void unassignedtable()
        {
            UnassignedCars.Text = "Select an Unassigned car";
            UnassignedCars.Items.Clear();

            //fills the unassigned table
            SQL.selectQuery("SELECT * FROM vehicle WHERE assigned LIKE 'blank'");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    UnassignedCars.Items.Add(SQL.see[0]);
                }
            }
        }

        private void unassignedinstructors()
        {
            UnassignedInstructors.Text = "Select an unassigned vehicle";
            UnassignedInstructors.Items.Clear();

            //fills the unassigned instructors
            SQL.selectQuery("SELECT inuser, infname FROM instructor WHERE inuser NOT IN (SELECT assigned FROM vehicle)");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    UnassignedInstructors.Items.Add($"{SQL.see[0]} {SQL.see[1]}");
                }
            }
        }

        private void assigned()
        {
            Assigned.Text = "Select a Car/Instructor Pair";
            Assigned.Items.Clear();

            //fills with assigned instructors
            SQL.selectQuery("SELECT i.inuser, i.infname, i.inlname, v.plate FROM instructor i, vehicle v WHERE i.inuser = v.assigned");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    Assigned.Items.Add($"{SQL.see[0]} {SQL.see[1]} {SQL.see[2]} - {SQL.see[3]}");
                }
            }
        }

        private void tomaintenance()
        {
            inmaintenance.Text = "Select a car in maintenance";
            inmaintenance.Items.Clear();

            //fills with assigned maintenance
            SQL.selectQuery("Select * FROM maintenance");

            if (SQL.see.HasRows)
            {
                while (SQL.see.Read())
                {
                    inmaintenance.Items.Add(SQL.see[0]);
                }
            }
        }

        private void TransitionalMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TransitionalMenu.SelectedIndex)
            {
                case 0:
                    Hide();
                    UpcomingAppointments toappts = new UpcomingAppointments();
                    toappts.ShowDialog();
                    Close();
                    break;
                case 1:
                    Hide();
                    AssignCar cars = new AssignCar();
                    cars.ShowDialog();
                    Close();
                    break;
                case 2:
                    Hide();
                    ConfirmUnavailability timeoff = new ConfirmUnavailability();
                    timeoff.ShowDialog();
                    Close();
                    break;
                case 3:
                    Hide();
                    DeleteStaff delete = new DeleteStaff();
                    delete.ShowDialog();
                    Close();
                    break;
                case 4:
                    Hide();
                    Statistics stats = new Statistics();
                    stats.ShowDialog();
                    break;
                case 6:
                    Hide();
                    Home home = new Home();
                    home.ShowDialog();
                    Close();
                    break;
            }
        }

        private void fillmenu()
        {
            List<string> transmenu = new List<string>();
            transmenu.Add("All Appointments");
            transmenu.Add("Assign Cars");
            transmenu.Add("Time Off Requests");
            transmenu.Add("Delete Staff Account");
            transmenu.Add("Company Statistics");
            transmenu.Add("--------------------");
            transmenu.Add("Logout");

            for (int i = 0; i < transmenu.Count(); i++)
            {
                TransitionalMenu.Items.Add(transmenu[i]);
            }
        }
    }
}
