﻿namespace DIA
{
    public static class CurrentClientInfo
    {
        public static string Username { get; set; }
        public static string Fname { get; set; }
        public static string Lname { get; set; }
        public static string Email { get; set; }
        public static string License { get; set; }
        public static int Hours { get; set; }
    }    
}
