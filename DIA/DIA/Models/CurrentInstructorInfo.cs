﻿namespace DIA
{
    public static class CurrentInstructorInfo
    {
        public static string Username { get; set; }
        public static string Fname { get; set; }
        public static string Lname { get; set; }
        public static string Email { get; set; }
        public static string Phone { get; set; }
        public static string Workertype { get; set; }
        public static bool deptmng { get; set; }
    }
}
