﻿namespace DIA
{
    public static class CurrentAdminInfo
    {
        public static string Username { get; set; }
        public static string Fname { get; set; }
        public static string Lname { get; set; }
        public static string Email { get; set; }
        public static string Phone { get; set; }
    }
}
